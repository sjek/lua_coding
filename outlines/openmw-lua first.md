- Engine handlers 
  - specifigs and use cases 
    - onupdate
    - onkeypress
    - oninit
    - onplayeradded

------------------------------------

- Events
  - serializable data
    - self.object
    - lists
  - non serializable
    - self
  - local and global events
    - local
    - global

------------------------------------

- Interfaces
  - where can be used
    - docs
  - definition ?

- Delayed actions
  - accessing leveled container
  - teleport ?
  - ways around
    - callbacks

- Console
  - luap, luas and luag
    - usage, examples

- Lua scripts usage
  - omwscript files
  - attach and remove

------------------------------------

- multiplayer aspects
  - multiple players
  - multiple loaded cells

------------------------------------

- accessing mwscript variables
  - locals on self
  - variable defined in mwscript
  - changing / reading
  - globals 

------------------------------------

- Journal ?

------------------------------------

- music and sounds ?
  - playing redetermined sound with id
  - playing music by region
    - list based ?

------------------------------------

- Magic
  - Effects and properties
    - set effects directly
    - get
      - enchantments?
  - spells
    - link to actor control part
    - set / get selected
  - magnitudes and modifiers
    - change / read ?

- Skills
  - as indicator ?
  - modifiers ?

- Attributes / dynamics
  - health
    - detecting death
  - depends and effects
    - speed
  - modifiers
    - game mechanics
      - base
      - currrent
      - damage

- Hardcoded info
  - Effects
  - Schools
  - Range
  - Stance

------------------------------------

- Input 
  - enginehandler ?
  - events ?
  - controller
  - keyboard

- UI (folder)
  - defining UI
    - health bar
    - map
    - inventory

------------------------------------

- Inventories and containers
  - fetching
  - search, string.find
    - records
      - best weapon
      - potions

- Creating records
  - model and texture paths
    - difference from openmw-cs
    - flashes
  - difference between craft and records
    - placement ability 
    - readings / changes ?

- Placing (world, inventories, container)
  - teleport
  - moveinto
  - unloaded cells ?

- enable, disable, delete
  - delay ?

- Projectiles ?
  - hit ?
  - rajectory ?

------------------------------------

- Controlling actors
  - AI dehardcoding yet to come 
  - Casting spells
    - target
    - direction?
  - Ai packages
    - combat
    - pursue
    - wander
  - Activating / consuming
    - potion
    - enchanted item / scrolls ?

------------------------------------

- Raycasts
  - cursor point
  - direction
  - collision ?

- Loading cells
  - searching for items
    - looping, lists

- Nearby
  - definition of nearby
  - actors
  - items
  - accessing containers
    - resolving

------------------------------------

- shaders (folder)
  - GLSL
  - Moving camera
