# openmw spell scripts, ideas and how to do

Spell effects have to be for now tied to certain spells made in the CS or in some cases can probably be tied to effect itself but this is not commonly reliable as per how morrowind works. Also quest stages, attacks, etc. are probably possible.

[[_TOC_]]

## spell types

1. usable
    1. Powers are usable once a day
    2. Spells 
2. constants
    1. diseases
    2. curses
    3. abilities

## Using in different ways

1. enchantment
2. directly using
3. keeping button down / channeling
4. runes / alchemy like combinations, magicka game
5. etc.

## lua hooks examples

1. Nearby
    1. Nearby on local
    1. actors, etc
2. types of objects
    1. Obj.type == Types.container, etc.
    1. Handler for object type or defined single or list
3. Dialogue
    1. Core api
4. Animation
    1. Handler or  animation package
5. cell(s)
    1. world
6. skills (existing and uses)
    1. Skillhandler or stats
7. Stats
    1. Dynamics ( health, fatigue, magicka )
8. faction 
9. AI
10. quests
    1. journal
11. location / area
    1. self, cell:findAll



## spells on actors / objects

### effects can be build upon how some spells already works like

1. detecting stuff with effects
2. adding things under night eye for puzzles or setting the room dark(?)
3. adding stat based bonuses
4. disintegration on equipments
5. adding negative or bonus effect based on another effect
6. nearby knockouts

### or effects can be scripted per affecting spell

1. scale of objects
    1. state machine
    2. limits
2. bound 
    1. objects appearing into world
        1. doors, items 
    2. equipments, ethereal or real
        1. armor
        2. weapons, etc.

3. forbid actions
    1. attack
    2. sneak
    3. magic
    4. teleport

4. changing object placements
    1. equipping to certain actor
        1. direct
        2. area
    2. looking for stuff nearby
        1. fetch
        2. effects teleport ie. detecting
    3. push out
        1. onUpdate, position vectors

5. affect animation
    1. knockout
        1. effect while knockout
        2. causing knockout
    2. Slowmo
    3. freezing
    4. using certain animations
        1. dancing
        2. sitting
        3. attack
        4. additional anim support

6. targeting different actors
    1. faction
    2. dialogue
    3. nearby
    4. Distance from target
    5. guild
    6. custom list

7. helpcall
    1. possible types
        1. followers
        2. guards
        3. daedra
        4. commoner ie. classes

    2. how side is defined
        1. by player stats
        2. or certain actor situation
            1. ie. quest stage
        3. lists, on runtime ?

8. camera movement
    1. pausing the world and moving ?
        1. world, camera
    2. following some other actor
        1. gaining control ?

9. placing VFX
    1. bones
        2. skyrin style spell hands
    2. placing object models
    3. difference to teleport ?

10. sounds
    1. make npc to say something
    2. changing the atmosphere
        1. bars
        2. bands playing / gigs
        3. greatness
        4. depression
        5. horror
    3. music possibility ?
        1. songs

11. style of effect
    1. fast / slow
    2. permament / temporary
    3. what's the magnitude, like with night eye
        1. is the efect pulsating between certain values
        2. can the values be randomised

## Placing things ( objects, vfx )

1. walls
    1. fires
    2. skeletons
    3. ice
    4. literally wall
2. summoning stairs
    1. objects
3. lights
    1. movement ?
        1. random vector
4. guard sentry
    1. AI follow or teleport
    2. magic casting ( MR ? ) 

5. placing buildings ( ie. ashlander architect )

6. raycasting the placement target
    1. vector math
    2. normal vector

7. clothes, equipment on 
    1. the actors
    2. inventories
    3. containers
    4. conditions ?

## summoning army

1. how to determine targets
    1. using ai packages
    2. player reputation
    3. factions
    4. quilds
    5. race, etc

2. what to summon, maybe a setting
    1. guards
    2. daedra
    3. common folk
    4. fargoths

3. how to do the placement
    1. replacing other objects / actors
    2. teleport to defined or random location
    3. creating placement coordinates
        1. grid placement
        2. random placement inside area
        3. by placed object in the game world

4. how to differentiate from summoned and natural
    1. placing vfx or object related to body part or nearby
    2. can different colorations be used by lua

## lua powered enchantments

1. triggers and conditions
    a.wield weapon
        i.reach
        ii.damage
    b.user / nerevarine / heirlooms
    c.using any or certain item
    d.area where enchantment is applicable
    e.by AI target (AI packages) 
    f.player target (hit api)

2. Effects
    a.nearbys
    b.user
    c.direct default effect (read access)
    d.poison enchantments
        i.by templating ?
        ii.is here a direct write access ? 

## Shaders

1. day / night cycle
2. skooma induced vision
3. invisibility weil
4. fireball

## mines

1. triggers
    1. distance
    2. types
    3. timer

2. effects
    1. nullified magic for certain time
        1. no magic stance ?
    2. damage
    3. teleport

## traps

1. types
    1. harmfull
        1. fire, icem etc
        2. making enemies
    2. benefical
        1. healing
        2. timely allies / conversion
        3. protection
        4. regeneration area
        5. night eye, etc. effects

2. visual effects
    1. VFX
        1. placement
        2. lasting time
        3. coupling with effect ?
    2. objects
        1. lights
        2. teleporting / moving

3. manual keys
    1. overwriting existing trap ?

4. trigger
    1. activation
    2. jumping
    3. wampirism
    4. time of the day

## available effects

- there's 141 effects hardcoded as morrowind mechanics
