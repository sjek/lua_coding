# Lua_Coding

Lua in general is coding geared to make it easy as i have understood and make it lightweight. This results in simlplicity but there's catches as with every language.

## lisence
- ON MY PART CC0 IS FINE BUT AS THERE'S OTHER PEOPLE INVOLVED HELPING, ASKING FOR ATTRIBUTION OR JUST MENTIONING WOULD BE NICE. 

## Getting started

- **(DISCLAIMER) some scripts, particularry with openmw api and tes3mp when getting into it, can contain bugs as the first goal is make them work.**

- This repository is kinda for documenting purposes and partial playground. 

There is also mindmap at Mindomo dealing with lua's general properties which i'm also trying to do.

https://www.mindomo.com/mindmap/lua-general-b156290905626a495ef543608c5bbfdd


## Lua in general 

- Mostly done the [dealing with tables](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/dealing%20with%20tables.md)

- Otherwise atm there is their own files for (these are more or less due to change)
    1. [If then](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/If%20then.md)
    2. [Loops](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/Loops.md)
    3. [Strings](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/strings.md)
    4. [Variables](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/Variables.md)
    5. [Printing (mostly for debugging)](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/Printing.md)
    6. [ipairs, pairs and #](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/ipairs%2C%20pairs%20and%20%23.md)
    7. [lua tables write and read](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/lua%20tables%20write%20and%20read.md)

- Plan is make also functions after which, i think the groundwork basics might be ready to move into api's

## thanks 

hebi

phoenix

Pharis

Urm

Ptmikheev

and other people on discord