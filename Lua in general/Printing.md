# printing values as example (mostly for debugging)

Basically this happens with print and loops.

I'll handle these with simple for loop and with ipairs and pairs

- for definition of strings
0
--------------------------------------

## print
This works like in the above examples simply taking the parameter to print inside () brackets, 
which can be indexes, strings, variables, ie. 
```lua
print(parameter)     
print(table[1])     -- index
print("something")  -- string
print(a)            -- variable defined elsewhere

print({10, 20, 30}) -- this doesn't work as 
                    -- it's not specified how the table should be printed
``` 

--------------------------------------

## For Loop

- [Definition](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/Loops.md)

>>>
As simple for looping from index i to a it can be written
```lua
For i=1, a=5, x=2 do
  print(table[i])
end
```
This needs the defining **i = 1** and **a = 5** 
as **without those the definition isn't complete.** 
ie.
```
i = starting index
a = ending index
x = increment, if not specified, defaults to 1 
```
>>>

so it can be written also like
```lua
for i=1, 5 do
  print(table[i])
end
```
this goes over index values from table[1] to table[5] and in this case prints them.

If two or more tables are needed to be printed side by side it can be done looping over each table.
```lua
for i=1, 5 do
  print(table[i], table2[i])
end
```
**This doesn't create the table itself tho.** 
For that ipairs is needed

## ipairs 

ipairs is function that 