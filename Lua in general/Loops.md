Looping in lua is very important and fundamental as there only tables so it's used very much.

- For stables see [dealing with tables](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/dealing%20with%20tables.md)

- For ipairs, pairs in above link and [it's own section](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/ipairs%2C%20pairs%20and%20%23.md)

## For loops 
is most basics of loops is lua.

Generally can be written
```lua
for a, b, c do
    something
end
```
where
```
a   starting index, must be defined
b   Ending index, must be defined
c   increment, defaults to 1 if not defined
```
so using this wrong is
```lua
x = { hat, pants }
for i, 2 do             -- i must be defined
    print(x[i])
end
```
and right **(also, just numbers can be used in place of variables)**
```lua
x = { hat, pants }
for i=1, 2 do             -- i is defined
    print(x[i])
end
```


## while loops