
## Tables:
First, a bit for the notation:

- --- **indexes starts from 1** ---

```lua
table = {"as", "is", "written"} 

as      --> 1
is      --> 2
written --> 3
```

### **So generally Tables itself are defined with {} brackets ie.** 
```lua
table = {}                            --empty, fillable table
table = {10, 20, 30}                  --table with numerals
table = {"game", "is", "on"}           --table with strings defined by " " 
table = {"game", "is", ["on"] = 5}    --Partially 2D table, 
                                        -- "game" has value 10
table = {"game", "is", "on", nil }        -- now one value is nil
```

When you want index from a table you must use **“ “** around strings. 

Otherwise if only index values are used, both the notations with **[" "]** or without are fine.

But you shouldn't give values directly to strings using only **" "** 

- --- **For cohesion is best to use always " " for strings and [" "] when giving values. works most of the time.** ---

  1) **Noteworthy is that x[i] skips 2D values counting/showing only 1D strings and nils.**

  2) This might affect loops

### syntax tests for above

```lua
x = {"good", "bad"} 	     -- index can be referenced as x[1]
print(x[1])   --> good        

x = {["good"] = 5, "bad"}  -- index value! referenced as x.good
print(x.good) --> 5
-- tho
print(x[1])   --> bad      -- so x[1] skips 2D value

but
x = {"good" = 5, "bad"}    --doesn’t work, errors!!!
               
and
x = {good, bad}            -- gives nil so doesn’t work!!!
print(x[1])

but 
x = {good = 5, bad}        -- this somehow works so is good
print(x.good)
print(x[1])                -- also nil in this case
```

.................................................................................

.................................................................................

## Using for loop with ipairs, pairs or #table to get length of the table ie.

In short the behaviour is as following:
```lua
ipairs     -- stops at 2D value or nil and reserves order
pairs      -- skips nils and count also 2D values but no definite order
#table     -- skips 2D values and counts also nils, gives the lenght
```
- for more [examples and definition in their own file](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/ipairs%2C%20pairs%20and%20%23.md)

- for [printing values as example (mostly for debugging)](https://gitlab.com/sjek/lua_coding/-/blob/main/Lua%20in%20general/Printing.md)

--------------------------------------

### Order

With order basically mean that when printing 
or otherwise using the table with these functions. 
The resulting order at which the values are given is either random or as given in the table. 

--------------------------------------

### ipairs

```lua
table = {"game", "is", ["on"] = 5, nil, "text" }  
n = 0
for _ in ipairs(table) do
  n = n + 1
end
```
This gives 2 stopping at 2D value

--------------------------------------

### pairs

```lua
table = {"game", "is", ["on"] = 5, nil, "text" }  
n = 0
for _ in pairs(table) do
  n = n + 1
end
```
This gives 4, skipping the nil

pairs is the most usable but has downside that the order isn't reserved 
so that can't be used in the code. Luckily only values can be searched? 
so this doesn't matte so much. 

--------------------------------------

### "#"

.. # which came with lua 5.1 as #table, 
basically gives directly the lenght of the table, while pair and ipairs as functions iterates over it. 

so
```lua
print(#table)

```
and (for the lenght itself this isn't needed but #table can be used as for loop ending parameter)
```lua
table = {"game", "is", ["on"] = 5, nil, "text" }  
n = 0
for i = 1, #table do
  n = n + 1
end
```
These both gives also 4, as this time 2D value is skipped. 





