```lua
-- functions

exam = {"chem", "phys", "eng"}

print(exam[1]) --> chem
print(#exam)   --> 3

function lenght(inp)
    local n = 0
        for _ , _ in pairs(inp) do -- to get entries
            n = n + 1
        end
    return n
end

--[[
function lenght(inp)
    n = 0
        for i=1 , #inp do   -- this works 
                            -- in terms of #
            n = n + 1
        end
    return n
end
]]

print(lenght(exam))

-- MORE TESTS

function ab(x)
    local total=0
    for i=1, x do        -- only works if x is numerical!!
        total=total + i
    end
    return total
end

print(ab(10))     -- only numerical values as above

--[[ Not this way

function name(var)
    local var = 0       -- variable should be input not defined
    for i=1, 6 do       -- normal loop from 1 to 6
        var = var + 1  
                    -- this doesn't do nothing special 
                    -- as var is overwritten as 0 before
    end
end
    
print(name(x))  -- again, nothing special as x is overwritten
    ]]

```