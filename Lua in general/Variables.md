Variables can be either local or global on which global is default, 
meaning that it can be used outside of function where it’s defined. 
**But not outside the file ?**

But first some notation. 

If there's no local keyword in froont of the variable it's global.
And can be written simple arithmetic

```lua 
x = 10
y = 20
p = x + y
print(p)   --> 30
```
defines global variables and sums them up, 
giving this value to variable p which is then printed. 
In short can also be written
```lua
print(10 + 20)   ---> 30
```

This time little time trip and using function to do the same 
```lua
function 
local x = 10
```
