#### with local and global
- local
```lua
local core = require('openmw.core')
local self = require('openmw.self')

local function onActive()
    core.sendGlobalEvent("bubbles", { appaself = self } )
end

local function onInactive()
    core.sendGlobalEvent("bubbleremove", { appaself2 = self } )
end

return { engineHandlers = { onActive = onActive, onInactive = onInactive } } 
```
- global
```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function funcname(var)
    local vfx = world.createObject("active_bubbles00", 1)
    vfx:teleport( var.appaself.cell.name, var.appaself.position )   
end

local function funcname2(var2)
    local cel = world.cells --[var2.appaself2.cell.name]
    for a, _ in pairs(cel) do
     if cel[a].name == var2.appaself2.cell.name then 
      local bubble = cel[a]:getAll(types.Activator)
       for i, _ in pairs(bubble) do
         if bubble[i].recordId == "active_bubbles00" then
            bubble[i]:remove()
         end
       end
     end
    end
end

return { eventHandlers = { bubbles = funcname, bubbleremove = funcname2 } } 
```
