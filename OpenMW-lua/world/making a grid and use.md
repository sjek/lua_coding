## this simply creates a grid in global grid on an event from player keypress

- PLAYER, this could use quest stage or something else 

```lua
local core = require('openmw.core')
local self = require('openmw.self')

local function onKeyPress(key)
    if key.symbol == 'c' then
      core.sendGlobalEvent("sjek_gridplacement", { data1 = self })
    end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

- GLOBAL, simply makes the grid and fill it with mushrooms
- point the number of spots and objects to match

```lua
local world = require('openmw.world')
local core = require('openmw.core')
local util = require('openmw.util')

local function functionname(func)

        local rooms = world.createObject( "flora_bc_mushroom_01" ,400)
                            -- objects to place, needed number
            local coor = {} 
            for i=1, 20 do
              for j=1 , 20 do
               local sta = rooms:split(1)  -- split for every spot
           
                coor = util.vector3(   -- makes the coordinates, corner and add
                      func.data1.position.x - 1000 + math.random(50,200)*i, 
                      func.data1.position.y - 1000 + math.random(50,200)*j,
                      func.data1.position.z )
              
                sta:teleport( func.data1.cell, coor ) -- place the objects                
              end
            end
end

return {
    eventHandlers = { sjek_gridplacement = functionname }
}
```
