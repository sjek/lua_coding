# This file will handle cells

[[_TOC_]]

## printing, accessing content on adding player

- this is dependant on MR https://gitlab.com/OpenMW/openmw/-/merge_requests/3017
- specifically world.cells

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function onPlayerAdded(player)
  local all = world.cells   -- get all the cells in the game
   for i, a in pairs(all) do    -- loop to check what to load
     
     if all[i].name == "Seyda Neen, Arrille's Tradehouse" then -- if by cell
     --if all[i].gridX == 10 and all[i].gridY == 10 then       -- if by cell coordinates
     --if all[i]:isInSameSpace(it.seob) then                   -- if by where something is (at least on player, active cell)
      
      local cont = all[i]:getAll(types.Container)  -- specific type, if actors, use NPC or 
      --local cont = all[i]:getAll()               -- everything
      
      for c, b in pairs(cont) do    -- loop to do stuff to content
        print(cont[c])
      end
     end
   end 
end

return { engineHandlers = { onPlayerAdded = onPlayerAdded } }
```

### This can also be done with attached script by openmwscript file on local

- ie. with onActive() enginehandler. 
- the needed cell needs to be loaded either by global script or going into cell

```lua
local self = require('openmw.self')

local function onActive()
print(self.recordId)     -- prints the object which have been become active to console
end

return { engineHandlers = { onActive = onActive } }
```

### if triggering from somewhere

- the problem that feced is that (because it's only on global):
- `self.object.cell:getAll(types.Static)` directly as i errors at getAll: 
- `attempt to call method 'getAll' (a nil value)` 

> so instead you need to do **loop in global** to get trought the cell or use the first example

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function functionname(so)

  local cel = world.getCellByName(so.endname.cell.name)  -- ie. self.object.cell.name
  local stati = cel:getAll(types.Static)  -- the wanted type
  for i, a in pairs(stati) do
    print(stati[i])           -- prints the wanted from list
  end
  
end

return {
    eventHandlers = { stage = functionname }  -- event is from player script sending self.object
}
```
- for fine tune the list you could make if statements inside the loop, before print

> there's string function string.find which gives true when found

- The **player script** is simply as trigger
```lua
local core = require('openmw.core')
local self = require('openmw.self')

local function onKeyPress(key)
           if key.symbol == 'c' then
              local firstname = self.object
              core.sendGlobalEvent( "stage", { endname = firstname } )
           end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

### finding keys

- one possibility for fine tuning is to find keys ie. **Global script**

```lua
  local cel = world.getCellByName(so.sel.cell.name)  -- ie. self.object.cell.name
  
  local stati = cel:getAll(types.Miscellaneous)  -- the wanted type
  
  for i, _ in pairs(stati) do  -- on open world
    if types.Miscellaneous.record(stati[i]).isKey then
      print(stati[i].recordId)  
          -- prints the wanted from list or do other stuff
    end
  end
  
  local wonp = cel:getAll(types.NPC)  -- needs NPC
  for l, _ in pairs(wonp) do
    local inv = types.Actor.inventory(wonp[l]):getAll(types.Miscellaneous)  -- inventories
      for k, _ in pairs(inv) do
        if types.Miscellaneous.record(inv[k]).isKey then  -- keys
          print(inv[k].recordId)
          print(wonp[l].recordId)  -- holder
        end
      end
  end
end
```

## there's a global part to use string.find

```lua
local world = require('openmw.world')

local function name(var)

  local ce = world.cells
  for i, a in pairs(ce) do   -- loops over cells
    if string.find( ce[i].name , var.sel ) then  -- search for given cell name, 
                                                 -- for example where something is 
      print(ce[i]) -- print's the matching cell(s)
                   -- or you can toggle something
    end
  end
  
end

return { eventHandlers = { stage = name } }
```