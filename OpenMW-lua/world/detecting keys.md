

#### detecting keys

- trigger, updated to send nearby lists to be without cell loop in global 
1. currently there is no timer or something to remove the effect
2. should add the timer or effects under onUpdate

```lua
local self = require('openmw.self')
local core = require('openmw.core')
local nearby = require("openmw.nearby")

local function onKeyPress(key)
           if key.symbol == 'n' then
              local items1 = nearby.items
              local actors = nearby.actors
              core.sendGlobalEvent( "keys" , { playerself = self.object, items = items1, nactors = actors } )
           end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

- this just gets the keys and summons smoke on them or carrier

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function funcname(var)

  local stati = var.items
  
  for a, _ in pairs(stati) do  -- on open world
    if stati[a].type == types.Miscellaneous then -- key is miscellaneous
      if types.Miscellaneous.record(stati[a]).isKey then
        print(stati[a].recordId)
        print(stati[a].position)
          -- prints the nearby keys or do some other stuff
         
         world.createObject( "steam_bluegreen", 1 ):teleport( stati[a].cell , stati[a].position ) 
      end
    end
  end
  
  local npcs = var.nactors
  for l, _ in pairs(npcs) do
    if npcs[l].type == types.NPC then
      local npcinventory = types.Actor.inventory(npcs[l]):getAll(types.Miscellaneous)
      for k, _ in pairs(npcinventory) do
        if types.Miscellaneous.record(npcinventory[k]).isKey then -- if holding a key
          
          print(npcinventory[k].recordId) -- the key
          print(npcs[l].recordId)         -- holder
          
          world.createObject( "steam_bluegreen", 1 ):teleport( npcs[l].cell , npcs[l].position )
        
        end
      end
    end
  end
end

return {
    eventHandlers = { keys = funcname }
}

```