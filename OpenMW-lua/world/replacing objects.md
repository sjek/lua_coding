# replacing world objects scriptically

[[_TOC_]]

## replacing every nearby NPC with flame atrocach

- in this case with keypress, player script

```lua
local nearby = require("openmw.nearby")
local core = require('openmw.core')

local function onKeyPress(key)

    if key.symbol == 'c' then  

      local near = nearby.actors
      core.sendGlobalEvent( "teleport" ,{ nearNPC = near }) 

    end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

- and in global

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function functionname(func)

    for i, a in pairs(func.nearNPC) do
      if func.nearNPC[i].type == types.NPC then
        world.createObject("atronach_flame", 1 ):teleport( func.nearNPC[i].cell , func.nearNPC[i].position )
        func.nearNPC[i]:remove()
      end
    end
end

return {
    eventHandlers = { teleport = functionname }
}
```

## subtly changing the name of already read books

- NOTE: this probably is lacking saving the books for eaxct savegame
- and the change is not instant as is

in GLOBAL script
```lua
local core = require('openmw.core')
local types = require('openmw.types')
local world = require('openmw.world')
local I = require('openmw.interfaces')
local async = require('openmw.async')

local activatedbooks = {}

I.Activation.addHandlerForType( types.Book, 
   async:callback(function(book, actor)
        if activatedbooks[types.Book.record(book).id] then  -- if the map table contains the book already
          --print("activated already")
        else
        activatedbooks[types.Book.record(book).id] = true -- this is needed exactly this to result to "D or map table
          --print(types.Book.record(book).id, "  tableted")
        end
end))


local function onObjectActive(object)

      if object.type == types.Book then   -- for books
       if activatedbooks[object.recordId] then  -- if in the activated table
         
          local booktemplate = types.Book.record(object)
          local booksname = { "is read", types.Book.record(object).name } 
                    -- for multiple strings to name 
          local booktable = {
                name = table.concat(booksname, " "), 
                template = booktemplate -- base tamplate
          }
          local recording = types.Book.createRecordDraft(booktable)
          world.createObject( world.createRecord(recording).id):teleport( object.cell, object.position, object.rotation )
          object:remove() -- remove the old one
        end
     end
end


return {
    engineHandlers = { onObjectActive = onObjectActive }
}
```