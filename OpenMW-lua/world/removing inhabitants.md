## these scripts remove the nearby actors in currently active (without distant land?) cells
- PLAYER: script
```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local core = require('openmw.core')

local function onKeyPress(key)
            if key.symbol == 'b' then
                ui.showMessage("something is happening")
                local acts = nearby.actors
                for b, c in pairs(acts) do
                    core.sendGlobalEvent("sarda", { source=acts[b].recordId, equ=acts[b] } ) 
                    -- defines the handler for global script and data for the script to access
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```
- GLOBAL:  script
```lua
local world = require('openmw.world')
local types = require('openmw.types')

local function is(it)
    print( tostring(it.source) )
    local inha = world.activeActors
    for i, a in pairs(inha) do
      inha[i].remove(it.equ)
    end
end

return {
    eventHandlers = { sarda = is } 
        -- "is" is dummy word which acts as a bridge between the event and it's values "it."
}
```
## there's `Lua error: Can't remove 0 of 0 items` for every instance, so there's probably better way than this, not enterin the same instanse twice 