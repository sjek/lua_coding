## There isn't much to handle in this altho possibilities are wide

### important point

> **The paths from openmw-cs cannot be directly copied.**
- it uses \ and lua can't handle this as it has meaning in coding
- so in lua you have to use `\\` or /

> items from recordcreation don't currently stack 

### for creating potions the used code snippet  

> there's other ones on https://gitlab.com/OpenMW/openmw/-/merge_requests/2944

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function functionname(ref)  -- using event  -- ref is just a name to use to access event's data

     local rec = {
          id = "p_fortify_strength_s",   
          name = "Standard Fortify Strength", 
          model = "m\\Misc_Potion_Standard_01.nif",
          icon = "m\\Tx_potion_standard_01.tga",
          mwscript = "",  
          weight = 0.750, 
          value = 35      
     }
     
     local ret = types.Potion.createRecordDraft(rec)
     local some = world.createRecord(ret)
     local item = world.createObject(some.id, 1)
     item:moveInto(types.Actor.inventory(ref.ta))  -- in this player from player script
end

return {
    eventHandlers = { eventname = functionname } 
 }
```
## for convenience here: placing the object in the game world

### for example placing chests under / at the player

- **any enginehandler that can send event should do on sender**
- in here **so.pos is the self.object from player** script to get position 
- **you need this in way or another**
- this seems to work also in exteriors.

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function functionname(so)
   local posi = so.pos.position 
   local skel = world.createObject( "com_chest_01" , 1 )
   skel:teleport(so.pos.cell , posi , { onGround = false } ) 
end

return {
    eventHandlers = { stage = functionname }
}
```
- the player script is simply
```lua
local core = require('openmw.core')
local self = require('openmw.self')

local function onKeyPress(key)
           if key.symbol == 'c' then
           core.sendGlobalEvent( "stage" , { pos = self.object } )
           end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```
- Or you can do this with onUpdate with a timer // TODO
