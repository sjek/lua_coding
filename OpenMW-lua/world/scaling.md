#### first if you just wanna set the scale on active

```lua
local core = require('openmw.core')

local function onObjectActive(object)
    if object.recordId == string.lower("recordID") then
        object:setScale(3)
    end
end

return { engineHandlers = { onObjectActive = onObjectActive } } 
```
#### for getting the scale tho you need to do it on onUpdate

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')


local function onUpdate(dt) 
-- onObjectActive(object) didn't work, probably uses only the initial scale, was nil
  
  if c == nil then -- this one is needed for c not to initialize while script runs
    c = 1
  end
  if b == nil then -- same as above
    b = 1
  end
  
  local lp = world.cells
    for i, _ in pairs(lp) do
      if lp[i].name == "Balmora, Council Club" then
        local misc = lp[i]:getAll(types.Miscellaneous)
        for a, _ in pairs(misc) do
          if misc[a].recordId == "misc_com_bottle_09" then
             
             if b == 1 or b == 4 then -- for initial and the conditional loop
             
                if misc[a].scale < 3 then -- for growing 
                print(tostring(misc[a].scale))
                  c = c + 0.1
                  misc[a]:setScale(c)
                  if misc[a].scale > 2.9 then -- the value didn't go over 3 
                     b = 2
                  end
                end
             end

             if b == 2 then  -- for making smaller
                --print(tostring(misc[a].scale))
                c = c - 0.1
                misc[a]:setScale(c)
                
                if misc[a].scale < 1 then -- back to growing
                  b = 4 
                end
                
              end
          end
        end  
      end
    end
end

return { engineHandlers = { onUpdate = onUpdate } } 
```