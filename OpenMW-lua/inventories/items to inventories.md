#### placing a weapon in the nearby actors inventory

- in the trigger script, local in this case 
- with ``nearby.actors`` loop

```lua
core.sendGlobalEvent( "sarda" , { nearbyactor = acts[b] } ) -- just the event, doesn't need self
```

- then in the GLOBal script 
```lua
local types = require('openmw.types')
local world = require('openmw.world')

local function isted(some)
    local ban = world.createObject( "banhammer_unique", 1 ) -- creates the object for scripts
    ban:moveInto(types.Actor.inventory(some.nearbyactor))   
        -- self syntax, the : , and moves the item into nearby actor gotten from event data 
end

return {
    eventHandlers = { sarda = isted }
}
```