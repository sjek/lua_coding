#### this changes the NPC's clothing. triggered
- trigger is for now a keypress
- thanks for an idea.

-   PLAYER:
```lua
local core = require('openmw.core')
local nearby = require('openmw.nearby')


local function onKeyPress(key)
   if key.symbol == 'b' then
      local nearactors = nearby.actors
      core.sendGlobalEvent("clothing", { near = nearactors })
   end 
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

- GLOBAL:
```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function functionname(var)

    local cloth = types.Clothing.records -- the clothing records in the world
    
      local clothes = {}      -- empty table for shirts
      for slot, item in pairs(cloth) do     -- for all clothes
        if cloth[slot].type == types.Clothing.TYPE.Shirt then -- if the cloth is shirt
          table.insert(clothes, item.id)      -- construct the table from bottoms up
        end
      end
 
      
        for a, _ in pairs(var.near) do      -- for every nearby actor  

          local rand = math.random(#clothes)    -- generates the random number from the amount of cloth records
              print(tostring(rand))    -- this works everytime givin a number
                 
              print( tostring ( types.Clothing.record(clothes[rand]).model )) -- ERRORS time to time
           local clothingmodel = types.Clothing.record(clothes[rand]).model  -- get the random shirt model
           
                 print(tostring(clothes[rand]))  
              local clothingTemplate = types.Clothing.record(clothes[rand])  -- gets the base template
          
              local clothingTable = {name = "better cloth" , template = clothingTemplate, model = clothingmodel }
                        -- the new cloth
    
                print( tostring(var.near[a].recordId), "....", tostring(types.Clothing.record(cloth[rand].id)))
    
              local recordDraft = types.Clothing.createRecordDraft(clothingTable)
              local newRecord = world.createRecord(recordDraft)
              local clothrecord = world.createObject(newRecord.id, 1)

              clothrecord:moveInto(types.Actor.inventory(var.near[a])) -- moves the cloth to inventory
          
              local equips = types.Actor.getEquipment(var.near[a])  -- this for not getting everybody naked
              equips[types.Actor.EQUIPMENT_SLOT.Shirt] = clothrecord.recordId
              
              var.near[a]:sendEvent("finalcloth", { equipments = equips } ) -- setequipment works only on local
          end         
end

return {
    eventHandlers = { clothing = functionname }
}
```

-   and the NPC:
```lua
local self = require('openmw.self')
local types = require('openmw.types')

local function name(var)
    types.Actor.setEquipment(self,var.equipments)
end

return {
    eventHandlers = { finalcloth = name }
}
```
