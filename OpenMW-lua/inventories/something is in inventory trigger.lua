local core = require('openmw.core')
local self = require('openmw.self')
local nearby = require('openmw.nearby')
local types = require('openmw.types')

local function onUpdate(dt)
    
    if types.Actor.inventory(self):countOf("p_reflection_e") > 0 and 
    types.Actor.inventory(self):countOf("p_levitation_s") > 0 then 

       core.sendGlobalEvent( "hasitemsname", { playerself = self, nearitems = nearby.items } )
       
    end
end

return { engineHandlers = { onUpdate = onUpdate } } 
