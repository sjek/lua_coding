
[[_TOC_]]

#### naked problem

- the naked script has a problem that it unequips all other equipmends by desing so needs an extra check
- basically that means adding / changing the wanted equipment for the right slot in a npc equipment table ie.
- in NPC script in place of ``types.Actor.setEquipment( self , {} )`` do this:

```lua
    local equipment = types.Actor.equipment(self)   -- equipments
        equipment[types.Actor.EQUIPMENT_SLOT.CarriedRight] = "banhammer_unique" 
                -- placing teh script created item 
        types.Actor.setEquipment(self, equipment)
                -- using the modified equipment table
```

#### you can print the used player equipments with
 
```lua
for i=1 , 18 do
    print("this is" .. " " .. i .. " " .. tostring(types.Player.equipment(self, i)))
end
```
------------------------------------
#### making the set from player

> This copies all weard player equipments and copies them to nearby npcs

- with discord help got code snippet to use and this end to be guide simple.
- just need the right approach
- point is that the equipment logic is done on global script and then the equipment list is sent to npc
- need two events still but pretty neat

------------------------------------

- **PLAYER:** 
- just gets the requied info to global and prints info

```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local types = require('openmw.types')
local core = require('openmw.core')

local function onKeyPress(key)
            if key.symbol == 'b' then -- the key needed to press
                ui.showMessage("something is happening") -- just random message
                local acts = nearby.actors
                local cer = types.Player.equipment(self)

                for i, a in pairs(cer) do
                    print(cer[i])       -- print the equipments
                end
                
                for b, c in pairs(acts) do -- loop
                    print(acts[b]) -- prints the nearby actors
                    core.sendGlobalEvent( "sarda" , { equ=acts[b],  armed = cer,  } ) 
                        -- sends the actor and player equipments to global script
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```
------------------------------------
- **GLOBAL:**
- this one is the neat part essentially creates the equipment table to send
- this is done by simple loop and the target got from player script is used
- for createoobject and moveinto one liner

```lua
local types = require('openmw.types')
local world = require('openmw.world')

local function isted(some)
  
  local equipment1 = some.armed     -- player equipments
  local inventory2 = types.Actor.inventory(some.equ) -- npc inventory
  local equipment2 = {}        -- creates fillable empty table
  
  for slot, item in pairs(equipment1) do    -- loops trough player inventory
      equipment2[slot] = item.recordId      
            -- pairs magic: 
            -- for every item.recordId in player inventory / it's table  
            -- places it on created table essentially copying
      world.createObject(item.recordId):moveInto(inventory2)
            -- this creates teh object and uses : to move it into npc inventory in single line
  end
  
  some.equ:sendEvent( "equip" , { inv = equipment2 } ) 
    -- sents the equipment list copied from player to local
end
```
------------------------------------
- **NPC:**
- only thing left here is to use the copied table for npc equipments

```lua
local types = require('openmw.types')
local self = require('openmw.self')

local function uneq(it)
         
        types.Actor.setEquipment(self, it.inv )
        -- this is funky part modifying self 
        -- ie. in this case the object script is attached to 
end

return {
    eventHandlers = { equip = uneq }
}
```
------------------------------------

#### full list of eguipment plots

- https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_types.html##(EQUIPMENT_SLOT)
- ie. in code this looks like this:

```lua
    local act = types.Actor.equipment(self)  
        -- self if the attached object is used. otherwise could? use event data

                -- this and these can be done to 
                -- types.Actor
                -- types.NPC
                -- types.Creature

            -- these are kinda copypasted. 
            -- to work in scripts, = must be defined
            -- so use the only needed ones
        act[types.Actor.EQUIPMENT_SLOT.Ammunition]
        act[types.Actor.EQUIPMENT_SLOT.Amulet]
        act[types.Actor.EQUIPMENT_SLOT.Belt]
        act[types.Actor.EQUIPMENT_SLOT.CarriedLeft]
        act[types.Actor.EQUIPMENT_SLOT.CarriedRight] = "banhammer_unique"
        act[types.Actor.EQUIPMENT_SLOT.Cuirass]
        act[types.Actor.EQUIPMENT_SLOT.Greaves]
        act[types.Actor.EQUIPMENT_SLOT.Helmet]
        act[types.Actor.EQUIPMENT_SLOT.LeftGauntlet]
        act[types.Actor.EQUIPMENT_SLOT.LeftPauldron]
        act[types.Actor.EQUIPMENT_SLOT.LeftRing]
        act[types.Actor.EQUIPMENT_SLOT.Pants]
        act[types.Actor.EQUIPMENT_SLOT.RightGauntlet]
        act[types.Actor.EQUIPMENT_SLOT.RightPauldron]
        act[types.Actor.EQUIPMENT_SLOT.RightRing]
        act[types.Actor.EQUIPMENT_SLOT.Robe]
        act[types.Actor.EQUIPMENT_SLOT.Shirt]
        act[types.Actor.EQUIPMENT_SLOT.Skirt]
```
- For the equipment slots numbers they can be get with
- luap on console and the using
- ``view(types.Actor.EQUIPMENT_SLOT)`` which gives

```
  Helmet = 0
  Cuirass = 1
  Greaves = 2
  LeftPauldron = 3
  RightPauldron = 4
  LeftGauntlet = 5
  RightGauntlet = 6
  Boots = 7
  Shirt = 8
  Pants = 9
  Skirt = 10
  Robe = 11
  LeftRing = 12
  RightRing = 13
  Amulet = 14
  Belt = 15
  CarriedRight = 16
  CarriedLeft = 17
  Ammunition = 18
```
