## this promply makes the companion share open when activating npc when not detected

### player script
```lua
local core = require('openmw.core')
local I = require('openmw.interfaces')


I.SkillProgression.addSkillUsedHandler(function(skillid, params)
    if skillid == 'sneak' and params.useType == I.SkillProgression.SKILL_USE_TYPES.Sneak_AvoidNotice then -- on succesfull 

     --print("sneak ok")
        core.sendGlobalEvent("sjek_pickpocket")    
  end
end)


local function event(var)
  I.UI.addMode('Companion', {target = var.record})
end

return {
    eventHandlers = { pickpocket_event2 = event }
}
```

### Global
```lua
local types = require('openmw.types')
local I = require('openmw.interfaces')
local async = require('openmw.async')
local core = require('openmw.core')
 
local function fname(var)
   I.Activation.addHandlerForType( types.NPC, 
    async:callback(function(NPC, actor)  
      
        --print(NPC.recordId)
        actor:sendEvent("pickpocket_event2", { record = NPC })
 
        return false
  end))
end

return {
    eventHandlers = { sjek_pickpocket = fname }
}
```

## all the possible ui modes
- needs tests which are possible in which scenarios

```
  Stats
  Trade
  Companion 
  Alchemy 
  Dialogue 
  Repair 
  SpellBuying 
  Travel 
  Recharge 
  Magic 
  MerchantRepair
  Container 
  Journal
  WaitDialog 
  Training 
  Inventory
  Scroll 
  Book 
  EnchantingDialog 
  JailScreen 
  LevelUpDialog 
  Map 
  QuickKeys 
  SpellCreationDialog
```