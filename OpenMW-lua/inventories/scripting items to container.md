# containers

[[_TOC_]]

## fisrt printing the contents upon activation, scrtipt attached via CONTAINER in openmwscript file

```lua
local types = require('openmw.types') 
local self = require('openmw.self')
local core = require('openmw.core') -- needed for getAll()

local function onActivated(actor)
    local invcon = types.Container.content(self.object):getAll()
        for b, v in pairs(invcon) do
        print(tostring(invcon[b]))
    end
end

return { engineHandlers = { onActivated = onActivated } }

```

## secondly adding items to container upon activation

### This is done via onActivated(actor) enginehandler so doesn't show the items directly to player

- as explained in
- https://gitlab.com/sjek/lua_coding/-/blob/main/OpenMW-lua/Explaining%20access.md#when-acceesing-container-and-placing-items

#### container script

- this script sends the container object info upon activation to global
```lua
local core = require('openmw.core')
local self = require('openmw.self')


local function onActivated(actor)
  local inv = self.object   
        -- don't use types here directly as it leads to "Value is not serializable" error
  core.sendGlobalEvent( "add" , { data = inv } )
end

return { engineHandlers = { onActivated = onActivated } }
```

#### Global script

- the wanted weapons are injected as a table via moveinto to activated container

```lua
local core = require('openmw.core')
local types = require('openmw.types')
local world = require('openmw.world')

local function is(it)

      -- local items = types.Weapon.records 
            -- this as is doesn't seem to work
      local items = { "6th bell hammer", "silver claymore", "steel dagger" }
            -- this creates table with wanted weapons
      for i ,a in pairs(items) do
        local arm = world.createObject( items[i] , 1 )  -- object creaion
        arm:moveInto( types.Container.content(it.data) )  
            -- this types is important to be here instead
      end
end

return {
    eventHandlers = { add = is }
}
```

### This can also be done only with global script but have the same problem as above

#### Global script using onActivate enginehandler

- this seems to automatically use the defined type for target

```lua
local types = require('openmw.types')
local world = require('openmw.world')

function onActivate(container, actor)  
  local items = { "6th bell hammer", "silver claymore", "steel dagger" } -- defines items
    for i, a in pairs(items) do
      local arm = world.createObject( items[i], 1 ) 
      arm:moveInto( types.Container.content(container) ) -- using the type needed
    end
  end

return { engineHandlers = { onActivate = onActivate } }
```

## making the item placement instant

>>>
- https://openmw.readthedocs.io/en/latest/reference/lua-scripting/interface_activation.html##(Activation).addHandlerForObject
- and
- https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_async.html##(async).callback
- are needed
>>>

- base from discord, **yuo need to have this script as GLOBAL**
- as mentioned here https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html?highlight=handler#script-interfaces


```lua
local types = require('openmw.types')
local I = require('openmw.interfaces')
local world = require('openmw.world')
local async = require('openmw.async')

   I.Activation.addHandlerForType( types.Container, -- or addHandlerForObject if you need it only for one specific container
    async:callback(function(container, actor)  
    
      local items = { "6th bell hammer", "silver claymore", "steel dagger" }
    
        for i, a in pairs(items) do
          local arm = world.createObject( items[i], 1 ) 
          arm:moveInto( types.Container.content(container) )  
        end
      -- add here `return false` if you want to disable standard activation
  end))
```
### for specifig container

- didn't get the object handler to work but one way to handle specifig object is by it's id. ie. 

```lua
   I.Activation.addHandlerForType( types.Container, -- or addHandlerForObject if you need it only for one specific container
    async:callback(function(container, actor)  
      if types.Container.record(container).id == "com_basket_01_ingredien" then
```

## picking random 10 armors to place

```lua
      local items = types.Armor.records
      
        for i=1, 10 do
          local rand = math.random(#items)
          local arm = world.createObject( items[rand].id, 1 ) 
          arm:moveInto( types.Container.content(container))  
        end
```