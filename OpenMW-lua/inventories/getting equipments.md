
- this script prints the equipment of nearby actors to console

- any local script can be used 
- or using global, via event for local or directly in it

```lua
local types = require('openmw.types')
local nearby = require('openmw.nearby')

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.symbol == 'v' then   -- key to be pressed
                 local near = nearby.actors
                 for b, c in pairs(near) do
                    local inv = types.Actor.equipment(near[b])  -- equipments list on individual actor 
                    for i, a in pairs(inv) do
                    print(tostring(inv[i]))     -- individual equipments
                    end
                 end
            end
        end
    }
}
```

```lua

```