
- as setequipment is only on self this needs two scripts
- for the event and the actual script

#### script

- this script is small as only needs to set setEquipment to "zero" with {}
- additionally prints the affected npc from the nearby list via event data

```lua
local types = require('openmw.types')
local self = require('openmw.self')

local function uneq(it)
    print( tostring(it.source) ) -- prints the sent source
    types.Actor.setEquipment( self , {} ) 
        -- this is funky part modifying self 
        -- ie. in this case the object script is attached to 
end

return {
    eventHandlers = { unequid = uneq }
}
```

#### trigger(s)

- from player
- can also be quest stage

```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')

local function onKeyPress(key)
            if key.symbol == 'b' then -- the key needed to press
                ui.showMessage("something is happening") -- just random message
                local acts = nearby.actors
                for b, c in pairs(acts) do -- loop
                  acts[b]:sendEvent( "unequid", { source=acts[b].recordId } )
                      -- source is not mandatory, just sends data with the event
                      -- the : is important, means lua's self for the used command/function
                      -- means acts[b].sendEvent( acts[b], "unequid", etc.
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }

```