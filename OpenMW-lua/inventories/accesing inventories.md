# nearby

[[_TOC_]]

## related to player

### printing the used equipments on nearby actors to console with pressing v key.
##### PLAYER:
- This script just does two loops, one for equipment lists and then fo equipments inside those lists
```lua
local types = require('openmw.types')
local nearby = require('openmw.nearby')

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.symbol == 'v' then   -- key to be pressed
                 local near = nearby.actors
                 for b, c in pairs(near) do
                    local inv = types.Actor.equipment(near[b])  -- equipments list on individual actor 
                    for i, a in pairs(inv) do
                    print(tostring(inv[i]))     -- individual equipments
                    end
                 end
            end
        end
    }
}
```
### getting everybody in active cells naked via keypress
- This needs two scripts because the setequipment works only on self 
##### PLAYER: 
- this just sends event to every nearby actor with help of a : syntax (self)
```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')

local function onKeyPress(key)
            if key.symbol == 'b' then -- the key needed to press
                ui.showMessage("something is happening") -- just random message
                local acts = nearby.actors
                for b, c in pairs(acts) do -- loop
                  acts[b]:sendEvent( "unequid", { source=acts[b].recordId } )
                      -- source is not mandatory, just sends data with the event
                      -- the : is important, means lua's self for the used command/function
                      -- means acts[b].sendEvent( acts[b], "unequid", etc.
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```
##### NPC:
- this script is small as only needs to set setEquipment to "zero" with {}
- additionally prints the affected npc from the nearby list via event data
```lua
local types = require('openmw.types')
local self = require('openmw.self')

local function uneq(it)
    print( tostring(it.source) ) -- prints the sent source
    types.Actor.setEquipment( self , {} ) 
        -- this is funky part modifying self 
        -- ie. in this case the object script is attached to 
end

return {
    eventHandlers = { unequid = uneq }
}
```
### placing a weapon in the nearby actors inventory
- in this case i'm only explaining the needed changes to above
- in PLAYER script you need to add this event in the loop. two events in same is fine it seems. 
- altho can possibly effect frame critical ones

```lua
core.sendGlobalEvent( "sarda" , { equ=acts[b] } ) -- just the event, doesn't need self
```

- and then in the GLOBAL script do this 

```lua
local types = require('openmw.types')
local world = require('openmw.world')

local function isted(some)
    local ban = world.createObject( "banhammer_unique", 1 ) -- creates the object for scripts
    ban:moveInto(types.Actor.inventory(some.equ))   
        -- this again, uses self syntax, and moves the item into nearby actor via event data 
end

return {
    eventHandlers = { sarda = isted }
}
```
- now all nearby actors have banhammer in their inventory and use it when AI calls it ?
- This script tho have a problem that it unequips all other equipmends by desing so needs an extra check
- basically that means adding / changing the wanted equipment for the right slot in a npc equipment table ie.
- in NPC script in place of `types.Actor.setEquipment( self , {} )` do this 

```lua
    local equipment = types.Actor.equipment(self)   -- equipments
        equipment[types.Actor.EQUIPMENT_SLOT.CarriedRight] = "banhammer_unique" 
                -- placing teh script created item 
        types.Actor.setEquipment(self, equipment)
                -- using the modified equipment table
``` 

- for that example the 
#### full list of equipment slots is here 
- https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_types.html##(EQUIPMENT_SLOT)
- ie. in code this looks like this:

```lua
    local eq = types.Actor.equipment(self)  
        -- self if the attached object is used. otherwise could? use event data

                -- this and these can be done to 
                -- types.Actor
                -- types.NPC
                -- types.Creature

            -- these are kinda copypasted. 
            -- to work in scripts, = must be defined
            -- so use the only needed ones
        eq[types.Actor.EQUIPMENT_SLOT.Ammunition]
        eq[types.Actor.EQUIPMENT_SLOT.Amulet]
        eq[types.Actor.EQUIPMENT_SLOT.Belt]
        eq[types.Actor.EQUIPMENT_SLOT.CarriedLeft]
        eq[types.Actor.EQUIPMENT_SLOT.CarriedRight] = "banhammer_unique"
        eq[types.Actor.EQUIPMENT_SLOT.Cuirass]
        eq[types.Actor.EQUIPMENT_SLOT.Greaves]
        eq[types.Actor.EQUIPMENT_SLOT.Helmet]
        eq[types.Actor.EQUIPMENT_SLOT.LeftGauntlet]
        eq[types.Actor.EQUIPMENT_SLOT.LeftPauldron]
        eq[types.Actor.EQUIPMENT_SLOT.LeftRing]
        eq[types.Actor.EQUIPMENT_SLOT.Pants]
        eq[types.Actor.EQUIPMENT_SLOT.RightGauntlet]
        eq[types.Actor.EQUIPMENT_SLOT.RightPauldron]
        eq[types.Actor.EQUIPMENT_SLOT.RightRing]
        eq[types.Actor.EQUIPMENT_SLOT.Robe]
        eq[types.Actor.EQUIPMENT_SLOT.Shirt]
        eq[types.Actor.EQUIPMENT_SLOT.Skirt]
```
- For the equipment slots numbers they can be get with 
- `luap` on console and the using
- `view(types.Actor.EQUIPMENT_SLOT)` which gives
```
userdata: 0x0253953564f0 {
  Helmet = 0,
  Cuirass = 1,
  Greaves = 2,
  LeftPauldron = 3,
  RightPauldron = 4,
  LeftGauntlet = 5,
  RightGauntlet = 6,
  Boots = 7,
  Shirt = 8,
  Pants = 9,
  Skirt = 10,
  Robe = 11,
  LeftRing = 12,
  RightRing = 13,
  Amulet = 14,
  Belt = 15,
  CarriedRight = 16,
  CarriedLeft = 17,
  Ammunition = 18,
} 
```
alternatively you can print the used player equipments with 
```lua
for i=1 , 18 do
    print("this is" .. " " .. i .. " " .. tostring(types.Player.equipment(self, i)))
end
```

### making the set up something specific
- ie. as above list can be used to define specific setup so i'm trying in this copy the player equipments.
- the simple use case for this is to move only pants and shirt when it's known they exist.
##### PLAYER:
- this becomes in this form somewhat complicated: 
- you need to define the correct slot numbers and then the right slots in player and npc script
- otherwise this works altho have still prints for debug purposes
```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local types = require('openmw.types')
local core = require('openmw.core')

local function onKeyPress(key)
            if key.symbol == 'b' then -- the key needed to press
                ui.showMessage("something is happening") -- just random message
                local acts = nearby.actors
                local cer = types.Player.equipment(self) -- fetches the player equipment table
                
                for i=1 , 18 do
                  print("this is" .. " " .. i .. " " .. tostring(types.Player.equipment(self, i)))
                end     -- for convenience prints the player equipments with slots 

                local pants2 = types.Player.equipment(self, 9).recordId  -- fetches specifig equipment in the form of recorid
                local shirt2 = types.Player.equipment(self, 8).recordId  
                    -- that recordid is important. without it the npc script tries to find the original item and nor created one

                local tab = {       -- table for easier processing
                    pants = cer[types.Actor.EQUIPMENT_SLOT.Pants].recordId, -- table entry for moveinto
                    shirt = cer[types.Actor.EQUIPMENT_SLOT.Shirt].recordId, -- these need to be correct slots
                 }

                 for b, c in pairs(acts) do -- loop
                    acts[b]:sendEvent( "unequid" , { source=acts[b].recordId, cool = shirt2, well = pants2 } ) 
                            -- local event for NPC 
                    
                    core.sendGlobalEvent( "sarda" , { equ=acts[b], armed = tab } ) 
                            -- for global event to moveinto
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }

```
##### GLOBAL:
- this script just 
```lua
local types = require('openmw.types')
local world = require('openmw.world')

local function isted(some)
    for i,a in pairs(some.armed) do  
            -- this loops over the manually created equipment table
            -- you can also use the player equpipments directly via event data
            -- with `some.armed[i].recordId` but this copies and transfers all equipments
      local ban = world.createObject( some.armed[i], 1 ) -- creates the equipments copy for scripts
      print(tostring(ban))
      ban:moveInto(types.Actor.inventory(some.equ)) -- moves it to nearby actors
    end
end

return {
    eventHandlers = { sarda = isted }
}
```
##### NPC:
- this is basically same as before but you need to define items that you want to equip
```lua
local types = require('openmw.types')
local self = require('openmw.self')
local core = require('openmw.core') 

local function uneq(it)
    print( it.source )  -- the npc in question
      local equipment = types.Actor.equipment(self) -- the modifiatable npc equipment list
        equipment[types.Actor.EQUIPMENT_SLOT.Pants] = it.well -- pants2
        equipment[types.Actor.EQUIPMENT_SLOT.Shirt] = it.cool -- shirt2
        
        types.Actor.setEquipment(self, equipment) -- uses the modified table
end

return {
    eventHandlers = { unequid = uneq }
}
```

### now let's try to to get all player equipments to transfer and equipped without manual fetching

- with discord help got code snippet to use and this end to be guide simple.
- just need the right approach
- point is that the equipment logic is done on global script and then the equipment list is sent to npc
- need two events still but pretty neat

#### player

- just gets the requied info to global and prints info

```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local types = require('openmw.types')
local core = require('openmw.core')

local function onKeyPress(key)
            if key.symbol == 'b' then -- the key needed to press
                ui.showMessage("something is happening") -- just random message
                local acts = nearby.actors
                local cer = types.Player.equipment(self)

                for i, a in pairs(cer) do
                    print(cer[i])       -- print the equipments
                end
                
                for b, c in pairs(acts) do -- loop
                    print(acts[b]) -- prints the nearby actors
                    core.sendGlobalEvent( "sarda" , { equ=acts[b],  armed = cer,  } ) 
                        -- sends the actor and player equipments to global script
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```

#### Global

- this one is the neat part essentially creates the equipment table to send
- this is done by simple loop and the target got from player script is used 
- for createoobject and moveinto one liner 

```lua
local types = require('openmw.types')
local world = require('openmw.world')

local function isted(some)
  
  local equipment1 = some.armed     -- player equipments
  local inventory2 = types.Actor.inventory(some.equ) -- npc inventory
  local equipment2 = {}        -- creates fillable empty table
  
  for slot, item in pairs(equipment1) do    -- loops trough player inventory
      equipment2[slot] = item.recordId      
            -- pairs magic: 
            -- for every item.recordId in player inventory / it's table  
            -- places it on created table essentially copying
      world.createObject(item.recordId):moveInto(inventory2)
            -- this creates teh object and uses : to move it into npc inventory in single line
  end
  
  some.equ:sendEvent( "equip" , { inv = equipment2 } ) 
    -- sents the equipment list copied from player to local

end
```

#### NPC

- only thing left here is to use the copied table for npc equipments

```lua
local types = require('openmw.types')
local self = require('openmw.self')

local function uneq(it)
         
        types.Actor.setEquipment(self, it.inv )
        -- this is funky part modifying self 
        -- ie. in this case the object script is attached to 
end

return {
    eventHandlers = { equip = uneq }
}
```
