
[[_TOC_]]

#### from standstill a chain 

```lua
local util = require('openmw.util')
local AIs = require('openmw.interfaces').AI

local function onUpdate(dt)
  
  if c == nil then -- sets up the c and doesn't thouch it again
    c = 1
  end
  
  if AIs.getActivePackage() == nil then -- this initializes whole thing (nothing at start)
    if c == 3 or c ==1 then    -- start and the place in loop
      print("p1")
      AIs.startPackage({
          type = "Travel",
          destPosition = util.vector3(-1231, 5566, -3227)
          })
      c = 2       -- second place
    end
    
    if AIs.getActivePackage() == nil and c == 2 then -- package check prevent firing and c keeps the place 
    print("p2")
      AIs.startPackage({
          type = "Travel",
          destPosition = util.vector3(273, 5209, -3244)
          })
       c = 3  -- in this case first again
    end 
  end
end

return { engineHandlers = { onUpdate = onUpdate }}
```
#### trigger

##### looping cells

- this might have the benefit that the travel script is always running

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function onUpdate(dt)
  local wcells = world.cells
    for i, _ in pairs(wcells) do
      if wcells[i].name == "cellname" then
        local anpc = lp[i]:getAll(types.NPC)  
          for a, _ in pairs(misc) do
          
            if anpc[a].recordId == string.lower("aaa_testnpc") then --lowercase
              
              anpc[a]:addScript("custom.lua")
              --:sendEvent('StartAIPackage', {type='Travel', destPosition = util.vector3( -1517, 5790, -3134 )}) -- for one package
            
            end
          
          end
        end
    end
end    

return { engineHandlers = { onUpdate = onUpdate } } 
```

##### on becoming active
- this has probably the downside that travel isn't preserved when outside of the cell
- so would need extra checks to preservation
```lua
local core = require('openmw.core')

local function onObjectActive(object)
    if object.recordId == string.lower("testnpc") then
      object:addScript("custom.lua")
    end
end

return { engineHandlers = { onObjectActive = onObjectActive } 
```