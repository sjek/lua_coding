
[[_TOC_]]

#### making all the nearby actors attack the player

- TODO: this can be done just by event 
- https://openmw.readthedocs.io/en/latest/reference/lua-scripting/aipackages.html 

```lua
local self = require('openmw.self')
local core = require('openmw.core')
local nearby = require('openmw.nearby')

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.symbol == 'v' then
              local acts = nearby.actors
              local targ = self.object
              for b, c in pairs(acts) do
                acts[b]:sendEvent( "add", { data = acts[b], ta = targ } ) --, { source=acts[b].recordId })
              end
            end
        end
   }
}
```
#### one way making the target random

- this can be possibly further improved by excluding player from targets
- otherwise the target seems to change periodically

```lua
local ai = require('openmw.interfaces').AI
local nearby = require('openmw.nearby')

function onActive()
  
  local acts = nearby.actors  -- this needs to be defined here
  for i, a in pairs(acts) do
    ai.startPackage({ type = 'Combat' , target = acts[i] })
  end
end

return { engineHandlers = { onActive = onActive } }
```


