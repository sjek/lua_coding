### these scripts, as is, gets nearby npc's to help in combat

- idea from discord

1.  worth pointing out that currently doesn't overwrite guard's behaviour if you attack npc for example
2.  needs 0.49 build
3.  this probably has edge cases on how the helping is determined, by getting the target in CREATURE, NPC script and sending NPC's to attack that instance
4.  seems to work for larger group of enemies

#### PLAYER: script
```lua
local nearby = require("openmw.nearby")
local core = require('openmw.core')
local self = require('openmw.self')
local I = require('openmw.interfaces')
local types = require('openmw.types')

I.AnimationController.addTextKeyHandler('spellcast', function(groupname, key) 
                                                        -- animation handler
    if key.sub(key, #key - 6) == 'release' then         
                                                        -- if the animation keyname has release in right spot
      if types.Actor.getSelectedSpell(self).id == "fireball" then 
                                                        -- the spell to use for helpcall
        local nearacts = nearby.actors
        for a, _ in pairs(nearacts) do                  -- for every nearby actor
          if nearacts[a].type ~= types.Player then      -- if not player
            --print(nearacts[a].recordId) -- prints them

              nearacts[a]:sendEvent("name_helpcall",{ selfa = self , near = nearacts }) 
                                                        -- the event 
          end
        end
      end
   end
end)  
```
#### CREATURE, NPC: script

```lua
local core = require('openmw.core')
local self = require('openmw.self')
local types = require('openmw.types')
local ai =require('openmw.interfaces').AI

local function funcname(var)
    if ai.getActiveTarget("Combat") then                -- if in combat and have a target?
      --print(ai.getActiveTarget("Combat").type, "is the target")
                                                        
      if ai.getActiveTarget("Combat").type == types.Player then
                                                        -- for actor attacking the player
        for a, _ in pairs(var.near) do
          if var.near[a].type == types.NPC then         -- for the helper to be NPC
            var.near[a]:sendEvent('StartAIPackage', {type='Combat', target = self })
                                                        -- attack
            print(var.near[a].recordId, "is siding with you")
          end
        end
      end
    end
end

return {
    eventHandlers = { name_helpcall = funcname }
}
```