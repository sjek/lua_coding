#### freezing actors in place // self:enableAI(false)

- This will freeze some of the melee attacks mid air,
- the casting animations run to end and
- actors can turn their upper bodies to look at player

#### remarks from oher self.controls  (for ruling out AI, it's set off)

```
1) ones working directly

-  self.controls.jump = true -- simply makes actor jump, also while moving along the moving direction
                                                                 
-  self.controls.sneak       -- toggles sneaking true / false
-  self.controls.use         -- uses the attack / spell readied
-  self.controls.pitchChange -- instant look up or down 
-  self.controls.yawChange   -- isntant rotation

2) ones needing actor movement

-  self.controls.run
-  self.controls.sideMovement
-  self.controls.movement   -- how this can be uses constantly .?
```