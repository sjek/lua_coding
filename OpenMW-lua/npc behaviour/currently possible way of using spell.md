#### triggered

- currently there isn't api to directly cast spells
- this code or mwscrip bridge cant be used

- for convenience async timers are used to make the execution
- tho for target you might need mwscript in either case

- TODO: Write the async timers

```lua
local self = require('openmw.self')
local types = require('openmw.types')
local ai = require('openmw.interfaces').AI

local function a1(setspell)
 types.Actor.spells(self):add("fireball")
 types.Actor.setSelectedSpell(self, 'fireball') -- these adds the spell and select it
end

local function a2(cast1)
  self:enableAI(true)
  ai.startPackage({ type = 'Combat' , target = cast1.ta }) -- could also be separated, isn't needed constantly
  self.controls.use = 1
          -- AI is still hardcoded so this can change in future
          -- setting stance get the AI to try to change but it's overwritten
          -- if the spell isn't setselected this uses the ai defined spell if it chooses spells

end

local function a3(cast2)
  self:enableAI(false)
  types.Actor.setStance(self, types.Actor.STANCE.Spell) -- this is needed for casting to start
  self.controls.use = 1
end

local function a4(whatspells)
 print(types.Actor.getSelectedSpell(self)) -- this works separated from setselected or on it's own
end

return {
    eventHandlers = { add1 = a1, add2 = a2, add3 = a3, add4 = a4 }
}

```

```lua

```