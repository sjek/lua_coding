## with trigger, places vfx to nearby targets

```lua

local nearby = require('openmw.nearby')
local core = require('openmw.core')
local util = require('openmw.util')
local types = require('openmw.types')

-- trigger here, timer, animationhandler, keypress, spell, etc.

local nearacts = nearby.actors  
    for a, _ in pairs(nearacts) do
     
     if nearacts[a].type == types.Creature then 
                -- the target(s)

        local effect = core.magic.effects.records["Sanctuary"] 
                -- wanted effect for static

        local pos = nearacts[a].position + util.vector3(0, 0, 100) 
                -- for setting the displacement

        core.vfx.spawn(effect.castStatic, pos)
                -- choosing and placing the vfx static
      end
    end

```

## placing vfx in hands while certain spell is choosen

```lua
local core = require('openmw.core')
local anim = require('openmw.animation')
local self = require('openmw.self')
local types = require('openmw.types')


local function onUpdate(dt)
    if types.Actor.getStance(self) == types.Actor.STANCE.Spell then
            -- in spell stance

      if types.Actor.getSelectedSpell(self).id == "fireball" then
            -- the spell chosen as trigger

        local effe = core.magic.effects.records[core.magic.EFFECT_TYPE.FireDamage].areaStatic
            -- what vfx to use

        anim.addVfx(self,effe,{bonename = 'Bip01 R Hand', loop = true, vfxId = "removeeffe"} )
        anim.addVfx(self,effe,{bonename = 'Bip01 L Hand', loop = true, vfxId = "removeeffe"} )
            -- placing the vfx to hands

      end
    else anim.removeVfx(self, "removeeffe")
            -- removing when the trigger is no more active
  end
end

return { engineHandlers = { onUpdate = onUpdate } }
```