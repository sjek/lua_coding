
- this doesn't need enginehandler if not wanted

```lua
local I = require('openmw.interfaces')

I.AnimationController.addPlayBlendedAnimationHandler(function (groupname, options)
   
   if groupname == "weapontwohand" then 
        -- for specifig group, weapontwohand in this case
      
    print(groupname)      
        -- prints the used group

    -- do stuff here, for example

      --local stop = options.stopkey -- key to print
        --print(stop)      

        options.speed = 0.2 -- does slow mo
   end
end)
```

- this doesn't currently affect the movement speeds like walking and jumping