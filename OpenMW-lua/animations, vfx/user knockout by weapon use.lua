local self = require('openmw.self')
local anim = require('openmw.animation')
local I = require('openmw.interfaces')
local types = require('openmw.types')

I.AnimationController.addTextKeyHandler('', function(groupname, key)
  if key == 'chop hit' then
    if types.Actor.getEquipment(self.object, types.Actor.EQUIPMENT_SLOT.CarriedRight).recordId == "nerevarblade_01_flame" then 
        anim.playQueued(self,"knockout",{ stopkey = "stop" ,speed = 1, loops = 0})
    end
  end
end)  