-- triggers can be kinda anything. for simplicity keypress is used. 

-- if the "n = 2" is removed the target (self) will fall down infinitely as the animation loops
-- if wanted, this can be prevented also with "loops = 0" option.

-- playqueud is used to prevent other animtions interfering. at least what tried. 
-- tho animation.clearAnimationQueue might also work in hat case

-- basically this just will knockout the self first to the ground and with second keypress going back up and jump.

-- original intention was to make animation play and stop to certain textkey 
-- while key is pressed, which seems to work besides the keyhold part.  

local self = require('openmw.self')
local anim = require('openmw.animation')

local n = 1

local function onKeyPress(key)
        if key.symbol == 'v' then
            if n == 1 then
              anim.playQueued(self,"knockout",{ stopkey = "stop" ,speed = 1, loops = 0})
              n = 2
            else
              anim.playQueued(self,"jump", {speed = 1, loops = 0})
              n = 1
            end
        end
end

return { engineHandlers = { onKeyPress = onKeyPress } }