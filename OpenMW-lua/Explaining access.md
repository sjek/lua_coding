This file is meant to explain how different fields in lua are accessed.

## Points being:

[[_TOC_]]

### Common errors

- You need to be exact on used libraries on local and global. 
- for example you can get `attempt to call method 'getAll' (a nil value)` using prohibitet function

### GameObject

### related types

### values with self can be accessed via types 

 ie.
- `speed = types.actor.RunsSpeed(self)`
- `pos = types.actor.position(self)`
- `rot = types.actor.rotation(self)` -- for example in Player script. 

**more general application of self is in lua**

- there's is for example sendevent which uses this
```lua
                local people = nearby.actors
                for b, c in pairs(acts) do
                    people[b]:sendEvent( "something" , { source=people[b].recordId })
                end
```
- where **:** denotes self shown in LDT autocompletion althought tooltips doesn't show it. 
- it means basically that function is executed in scope of self  
- other way writing this would be `people[b].sendEvent( people[b], "something" , { source=people[b].recordId })`

### when accessing container and placing items in it 

- is important to note that when using `onActivated(actor)` on local container script 
- the content of the container is shown to the player (also npc?) first, before script is run
- so it can't be used to place items to containers for use.

### using types.Actor.getSelectedSpell(self).Id

- using this for comparison ie. checking for certain spell the ``.Id`` end 
- is crucial for checking against core.magic.spells["spell"].Id

### The mwscript variables  // TODO