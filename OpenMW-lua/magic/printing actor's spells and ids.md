- Player 
```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local core = require('openmw.core')

local function onKeyPress(key)
            if key.symbol == 'n' then
                ui.showMessage("minds are read")
                local acts = nearby.actors
                for b, c in pairs(acts) do
                    core.sendGlobalEvent("spells", { source=acts[b].recordId , equ=acts[b]} )
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```
- Global
```lua
local types = require('openmw.types')

local function is(it)
    print( tostring(it.source) .. "---------------" .. "NPC" )
      local mySpells = types.Actor.spells(it.equ)  -- this is the list of spells on individual actor
      for _, somet in pairs(mySpells) do -- loop trought the list 
        print(somet.id)     -- uses id from types for somet dummy
      end
end

return {
    eventHandlers = { spells = is }
}
```