
## difference between spells and effects currently as of 11.2.2024

- spells are applied like normal spells in the game with ui and all
- effects are just effects without the ui mark and it varies wha effects gameplay

- following PLAYER script adds every effect on the game with value 20 and effects from spells and pure effects can be checked. 

> test result: every effect in the game is recorded after set, althought all don't manifest, and not recorded by spell effects

```lua
local self = require('openmw.self')
local types = require('openmw.types')
local core = require('openmw.core')


local function onKeyPress(key)
   if key.symbol == 'b' then

      local effe = core.magic.effects
      for a, _ in pairs(effe) do
        types.Actor.activeEffects(self):set(20,effe[a].id)
                    -- this adds every effect on game
      end

   end

   if key.symbol == 'n' then
      local effea = core.magic.spells
      for i, _ in pairs(effea) do
        if types.Actor.activeSpells(self):isSpellActive(effea[i].id) == true then
          local loef = effea[i].effects
          for d, _ in pairs(loef) do
            print(loef[d].effect.id)   -- this prints affecting spell effects 
          end
        end
      end
   end


   if key.symbol == 'h' then
      local effea = core.magic.effects
      for i, _ in pairs(effea) do
        if types.Actor.activeEffects(self):getEffect(effea[i].id).magnitude == 20 then
          print(types.Actor.activeEffects(self):getEffect(effea[i].id))
                    -- this is used to print pure effects
        end
      end
   end
   
end

return { engineHandlers = { onKeyPress = onKeyPress } }
```