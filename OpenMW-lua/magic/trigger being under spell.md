
[[_TOC_]]

#### Player script

- **The only problem is that remove is called multiple times but haven't ill effect if not for performance**
- needs more deliberate cell check for removal ie. loop all cells as as is this doesn't remove leftovers when going out 
```lua
local core = require('openmw.core')
local self = require('openmw.self')
local types = require('openmw.types')


local function onUpdate(dt)

  if c == nil then --set the initial value for comparison ie. not nil
    c = 1   
  end

  if c >= 1 then  --  will fire initially and math drops c to 0
                  --  if having spell drops the c multiple times (from 2 or 3)
     if types.Actor.spells(self)["night-eye"] == nil then -- if haven't spell
       print("remove")
        core.sendGlobalEvent("removeBubbless", { pself2 = self }) -- do stuff
        c = c - 1   
     end
  end
    
  if c <= 1 then  -- will fire initially and math raises c to 3
                  -- if don't have the spell initially the c raises to 2
                  -- fires only once in bot h cases 
     if types.Actor.spells(self)["night-eye"] then  -- if have the spell
        print("add")
        core.sendGlobalEvent("addBubbless", { pself = self }) -- do stuff
        c = c + 2
     end
  end
    
end

return { engineHandlers = { onUpdate = onUpdate } }
```

#### the effect in this case placing and removing the bubbles

```lua
local world = require('openmw.world')
local types = require('openmw.types')
local core = require('openmw.core')

local function funcname(var)
    
   local cont = var.pself.cell:getAll(types.Container)
    local bubblesone = world.createObject("active_bubbles00", #cont )       
      for a, _ in pairs(cont) do
        local bubbles = bubblesone:split(1)
        bubbles:teleport( var.pself.cell.name, cont[a].position )
      end
end


local function func2(var2)
    local cel = world.cells 
    for a, _ in pairs(cel) do
     if cel[a].name == var2.pself2.cell.name then -- this doesn't work from outside of the cell by design
                                                  -- and doesn't loop the creations so busted
                                                  -- so either specifig cell or everything in the world
      local bubble = cel[a]:getAll(types.Activator)
       for i, _ in pairs(bubble) do
         if bubble[i].recordId == "active_bubbles00" then
            bubble[i]:remove()
            print("remove")
         end
       end
     end
    end
end


return { eventHandlers = { addBubbless = funcname, removeBubbless = func2 } } 
```
#### just a cell check

- first check needs lines like these

```lua
    local cel = world.cells 
    for a, _ in pairs(cel) do
       if cel[a].name == "Cell name" then  
   
        local cont = cel[a]:getAll(types.Container)

        -- and teleport
        bubbles:teleport( "Cell name", cont[a].position )
```
- second check just need to check for specifig cell ie. 
```lua
      if cel[a].name == "Cell name" then
```