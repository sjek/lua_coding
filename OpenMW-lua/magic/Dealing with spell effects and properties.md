# This file in mostly on access

[[_TOC_]]

## spell effect info can be accesses with core.magic.effects

```lua
              local spel = core.magic.effects
              for a, b in pairs(spel) do
                if spel[a].name == "Levitate" then
                  print(spel[a].baseCost)
                end
              end
```

## printing actor's spells via global ( and subcategories ) 

- it's very likely possible run this on different enginehandler in one script, maybe also on local

- for this there's more nesting. The player script is as trigger and gives nearby actors and their recordId
- basically fetches the underlaying structure with loop in loop


```lua
local types = require('openmw.types')
local core = require('openmw.core')

local function is(it)
    print( tostring(it.source) .. "---------------" .. "NPC" )
      
      local mySpells = types.Actor.spells(it.equ)   -- spells on actor
      for i, somet in pairs(mySpells) do 
          if mySpells[i].type == core.magic.SPELL_TYPE.Ability then -- get abilities from spells
            print("name" .. " " .. mySpells[i].name)    -- prints the corresponding spell name
                                                        -- 1)

            local effe = mySpells[i].effects  -- using effects as it's the only table
            for c, b in pairs(effe) do    -- loops over effects under abilities in this case
              print(effe[c])              -- prints the effects table, can't be used with ..
                                          -- 2) 

              print("area" .. " " .. effe[c].area)
              print("school" .. " " .. effe[c].effect.school) -- 3)
            end
          end  
      end
end

return {
    eventHandlers = { spells = is }
}
``` 

- **do note that not all fields are in everything and they gives just a nil as empty**

>
> - ------ for **SPELL_TYPE** there's these values
> - **Ability, Blight, Curse, Disease, Power, Spell**
> - https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_core.html##(SpellType)
>
> **1)** you can do `print("cost" .. " " .. mySpells[i].cost)` from here
> - **cost, effects table, id, name, type**
> -    https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_core.html##(Spell)
>
> **2)** in there it's possible to access these
> -  **affectedattribute, affectedskill, area, effect table, magnitudeMax, magnitudeMin, range**
> -    https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_core.html##(MagicEffectWithParams)
>
> **3)** last stage what can be currently accessed is: for example `effe[c].effect.id` 
> - **baseCost, color, harmfull, id, name, school**
> -    https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_core.html##(MagicEffect)
>
 
#### school, effect, id values can be got in console at player mode

- https://gitlab.com/sjek/lua_coding/-/blob/main/OpenMW-lua/spell%20effects%2C%20schools%2C%20Range%2C%20stance%20hardcoded%20info.md


## accessing the effects on actor, instead of a spell list 

- dependant on https://gitlab.com/OpenMW/openmw/-/merge_requests/3036 

> for example changing actor / player speed while levitating, needs remove when effect ends if wanted

```lua
            local spe = types.Player.activeEffects(self):getEffect("Levitate")
               if spe then
                types.Actor.stats.attributes.speed(self).modifier = 100
               end
           end
```