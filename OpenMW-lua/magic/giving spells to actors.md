## spells and stuff

[[_TOC_]]

#### all to all
- Player
```lua
local ui = require('openmw.ui')
local nearby = require('openmw.nearby')
local core = require('openmw.core')

local function onKeyPress(key)
            if key.symbol == 'b' then
                ui.showMessage("warlock is here")
                local acts = nearby.actors
                for b, c in pairs(acts) do
                    core.sendGlobalEvent("addspells", { source=acts[b].recordId , equ=acts[b]} )
                end
            end
end

return { engineHandlers = { onKeyPress = onKeyPress } }

```
- Global
```lua
local types = require('openmw.types')
local core = require('openmw.core')

local function is(it)

    print( tostring(it.source) .. "           " .. "NPC" )

      local mySpells = types.Actor.spells(it.equ) 
        -- it.equ must be here. doing add directly doesn't work as need spell list
        -- this loop itself is copy pasted from docs. 

      for _, spell in pairs(core.magic.spells) do    
                -- loops trought all spells in the game

        if spell.type == core.magic.SPELL_TYPE.Spell then
                -- checks the added spell type. 
                -- type can be changed if needed
                -- eg. giving all abilities for example

          mySpells:add(spell)
                -- uses the pairs loop dummy variable for assigning the spell
        end
      end
end

return {
    eventHandlers = { addspells = is }
}

```
#### all to all but simpler
- modification to player
```lua
                for b, c in pairs(acts) do

                    acts[b]:sendEvent( "addspells" , { source=acts[b].recordId })
                        -- here : denotes lua self, which is in many functions. 
                        -- otherwise works similar to Global event
                end
```

- local on NPC

```lua
local types = require('openmw.types')
local core = require('openmw.core')
local self = require('openmw.self')

local function is(it)
    print( tostring(it.source) .. "      " .. "NPC" ) 
      -- print the actor id via event. can also be written
      -- print(self.recordId  .. "      " .. "NPC" ) 

    local mySpells = types.Actor.spells(self)
      -- contains self instead of it.equ. for this self must be also required

      for a , spell in pairs(core.magic.spells) do

        if spell.type == core.magic.SPELL_TYPE.Spell then
              -- this can be changed to different type if wanted
              -- these are Ability, Blight, Curse, Disease, Power and Spell

          mySpells:add(spell)
        end
      end
end

return {
    eventHandlers = { addspells = is }
}

```
- if for specifig npc or actors then better to use addscript, getting object via event or loop

#### specifig spells

- in global from player event and nearby actors 

```lua
local types = require('openmw.types')
local core = require('openmw.core')

local function is(it)
    print( tostring(it.source) .. "-----------------------" .. "NPC" )

      local mySpells = types.Actor.spells(it.equ)  -- for local change this to self and require self

      local tab = { "levitate" , "jump" }   -- the wanted spells
      for i, spell in pairs(tab) do

        mySpells:add(tab[i])  -- this is different, need the i as it's fetching and not assigning
      end
end

return {
    eventHandlers = { addspells = is }
}

```
#### giving chameleon while sneaking
- in local script 
```lua
local self = require('openmw.self')
local types = require('openmw.types')

local function onUpdate(dt)         -- on every frame

    if self.controls.sneak == false then      -- ensures that chammeleon goes off when not sneaking
                                              -- the result value true/false, not a number

      types.Player.spells(self):remove("ghost ability")

    elseif self.controls.sneak == true then   -- chameleon ability when sneaking
      types.Player.spells(self):add("ghost ability")

    end
end

return { engineHandlers = { onUpdate = onUpdate } }
```

- this can be done when the scriptholder is in cast stance simply by

```lua
local self = require('openmw.self')
local types = require('openmw.types')

local function onUpdate(dt)
    if types.Actor.stance(self) ~= 2 then       -- when the stance is not magic
      types.Player.spells(self):remove("ghost ability")
      
    elseif types.Actor.stance(self) == 2 then   -- when in magic stance
      types.Player.spells(self):add("ghost ability")
    end
end

return { engineHandlers = { onUpdate = onUpdate } }
```

#### making every attack hit

```lua
local self = require('openmw.self')
local types = require('openmw.types')

local function onActive()
  types.Actor.activeEffects(self):set(100,"FortifyAttack")
end

return { engineHandlers = { onActive = onActive } }
```
```lua

```