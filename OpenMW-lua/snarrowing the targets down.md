// TODO
```
-- get npc's on specific race
-- weapons of specifig range
-- all types from a distance from something (official doc example uses lenght)
-- books of specifig skill 
-- etc.
```

#### generally 

> if you'r accessing directly on local script you can use self.
> ie.

```lua
if types.Actor.isOnGround(self) == false then
        -- maybe levitation effect
end
```
> if you'r accessing via event you can compare to sent value 
> and get **the records properties via types**

```lua
-- quick theoretical example
-- with a loop over inventory
if types.Miscellaneous.record(inv[i]).isKey == true then
    if  types.Miscellaneous.record(inv[i]).id == "string from event" then
        -- messagebox for quest
    end 
end
```