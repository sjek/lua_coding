
this script will make the aimed npc or creature under attack when doing chop hit with nerevearine blade

```lua
local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local ui = require('openmw.ui')
local core = require('openmw.core')
local self = require('openmw.self')
local I = require('openmw.interfaces')
local types = require('openmw.types')
local async = require('openmw.async')


I.AnimationController.addTextKeyHandler('', function(groupname, key)
  if key == 'chop hit' then
    if types.Actor.getEquipment(self, types.Actor.EQUIPMENT_SLOT.CarriedRight).recordId == "nerevarblade_01_flame" then 

     local centervect = camera.viewportToWorldVector(util.vector2(0.5,0.5)):normalize()
     local pos = camera.getPosition()

     local nearacts = nearby.actors
    
        nearby.asyncCastRenderingRay( async:callback(function(rayC) 
          
          if rayC.hitObject then
            if rayC.hitObject.type == types.NPC or rayC.hitObject.type == types.Creature then

            ui.showMessage( tostring( rayC.hitObject ) )
                for a, _ in pairs(nearacts) do
                   nearacts[a]:sendEvent('StartAIPackage', {type='Combat', target = rayC.hitObject })
                end
            end
          end
        end) ,pos, pos + centervect * 20000)
    
    end
  end
end)  
```