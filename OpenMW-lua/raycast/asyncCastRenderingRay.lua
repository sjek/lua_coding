
local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local ui = require('openmw.ui')
local async = require('openmw.async')

-- this needs a trigger or enginehandler

    local centervect = camera.viewportToWorldVector(util.vector2(0.5,0.5)):normalize()
    local pos = camera.getPosition()

   nearby.asyncCastRenderingRay( async:callback(function(rayC) 

        ui.showMessage( tostring( rayC.hitObject ) ) -- for example for player
        -- do stuff here

    end) ,pos, pos + centervect * 20000)
