

[[_TOC_]]

## possible options

1)  trigger target to cast magic (needs setup)
2)  give spells, abilities, diseases... to the target
3)  detect vampirism
4)  teleport somewhere ?
5)  make target to attack, pursue, follow...
6)  affect stats, attributes and skills
7)  etc.

## getting the object in front of the player (player script)

> - basically this means to get where the cursor is pointing at.
> - so isn't necessarily the right or best solution
> - if you want something specifically targeting or ingnoring is available as options

- https://gitlab.com/sjek/lua_coding/-/blob/main/OpenMW-lua/raycast/castRay.lua


## modification to above

```lua
      core.sendGlobalEvent( "effects" ,{ hitposition = rayC.hitPos, playerself = self })
```

### placing objects

- flame atcronach
```lua
local world = require('openmw.world')
local core = require('openmw.core')

local function funcname(var)
    world.createObject("atronach_flame", 1 ):teleport( var.playerself.cell , var.hitposition )
end

return {
    eventHandlers = { effects = funcname }
}
```

## filtered nearby.actors can be better for some uses 
- for example if object is directly under player 

```lua
local acts = nearby.actors 
    for i, a in pairs(acts) do
        local dist = (self.position - acts[i].position):length() -- the distance to actor                
            if dist < 100 and dist > 10 then  -- the max distance to actor, and min over 0 to prevent player
                -- do stuff
            end
    end
```
## stealing spells

```lua
      local npcspells = types.Actor.spells(rayC.hitObject)
      
      for a, _ in pairs(npcspells) do
        print(npcspells[a])
        types.Actor.spells(self):add(npcspells[a])
      end
```
