local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local ui = require('openmw.ui')

local function onKeyPress(key)
    if key.symbol == 'c' then

    local centervect = camera.viewportToWorldVector(util.vector2(0.5,0.5)):normalize()
                                        -- vector from center of the screen

      local pos = camera.getPosition()  -- player's camera position
      local rayC = nearby.castRay(
                pos, 
                pos + centervect * 20000, {
                collisionType = nearby.COLLISION_TYPE.Actor } 
            ) 
                                  
            if rayC.hitObject then      -- may be better than nil check, ie. if object exixts
                print(rayC.hitObject.recordId)     -- the actor's recordId 
            else
              ui.showMessage("target not found")    -- or return, which wouldn't give nothing tho
            end
    end
end

return { engineHandlers = { onKeyPress = onKeyPress } }

--[[ old one
        pitch = -(camera.getPitch() + camera.getExtraPitch())
        yaw = (camera.getYaw() + camera.getExtraYaw())      -- these are from https://www.nexusmods.com/morrowind/mods/52202
        
        local xzLen = math.cos(pitch)
        local bor = util.vector3(
          xzLen * math.sin(yaw), -- x
          xzLen * math.cos(yaw), -- y
          math.sin(pitch)        -- z     -- and this angle calculus is from zackhasacat in discord
         ):normalize()        
]]