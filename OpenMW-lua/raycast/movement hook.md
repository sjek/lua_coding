## this script basically fire a hook (in metaphorical sense)

- detects if specifig weapon is equipped and on hit (the peak of attack animation)(on chop in this case) the rest of the script is triggered.
- PROBLEMS: 
1.  clips inside objects and physic is present so maybe need to move only the camera and then teleport player.
2.  if the target isn't reached, the move will continue infinitely
3.  currently needs luareload for default character as equipment is first nil

### in PLAYER script
```lua
local camera = require("openmw.camera")
local nearby = require("openmw.nearby")
local util = require("openmw.util")
local core = require('openmw.core')
local self = require('openmw.self')
local I = require('openmw.interfaces')
local types = require('openmw.types')
local async = require('openmw.async')


if types.Actor.getEquipment(self.object, types.Actor.EQUIPMENT_SLOT.CarriedRight).recordId == "nerevarblade_01_flame" then
                                                            -- recordId specifically
  I.AnimationController.addTextKeyHandler('', function(groupname, key)
   if key == 'chop hit' then  -- the animation key to react to

      local centervect = camera.viewportToWorldVector(util.vector2(0.5,0.5)):normalize()
      local pos = camera.getPosition()

        nearby.asyncCastRenderingRay(  async:callback(function(rayC)
            -- async callback works by assigning the result to function valua, or something 
      
        core.sendGlobalEvent("thehook",{ target = rayC.hitPos , selfa = self, vectormove = centervect})
            -- usage inside async:callback

        end) ,pos, pos + centervect * 20000) -- the rest of renderingray
      
    end
  end)
end    
```

### in GLOBAL script
```lua
local time = require('openmw_aux.time')
local core = require('openmw.core')
local util = require('openmw.util')

local function fname(var)
    
    stoptimerkey = time.runRepeatedly(function()
    
    local newpos =  var.selfa.position  -- this presumably takes player positions, somehow, as it works
    var.selfa:teleport(var.selfa.cell, util.transform.move(var.vectormove * 100):apply(newpos), { onGround = true } )
                                                                                -- onGround is messing with physics
    if (var.selfa.position - var.target):length() < 200 then
      stoptimerkey()    -- stops the timer if near enough of "hook" 
    end
    
    end ,0.1 * time.second )
    
end

return {
    eventHandlers = { thehook = fname } 
}
```