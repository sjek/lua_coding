I write here for now quick examples as i go. need to later format and organise better like with lua general points

## using onUpdate as a timer (this took a while to spot) (also on timers)

```lua
local core = require('openmw.core')
local self = require('openmw.self')

local n = 0

local function onUpdate(dt)
  n = n + dt   -- adds seconds(frametimes) to n
  --print(tostring(n)) 
  if n > 2 then
    core.sound.say("sound/cr/were/wolf growl 14.wav",self,"we are nearby")
    n = 0
  end
end

return { engineHandlers = { onUpdate = onUpdate } }
```

## making the attacks always hit (script on affected)

```lua
local self = require('openmw.self')
local types = require('openmw.types')

local function onActive()
  types.Actor.activeEffects(self):set(100,"FortifyAttack")
end

return { engineHandlers = { onActive = onActive } }
```

## this script prints all loaded static records to console when player is added to game world
```lua
local types = require('openmw.types')

return {
    engineHandlers = {
        onPlayerAdded = function()
            local obj = types.Static.records -- this fetches all loaded statics
            for i,a in pairs(obj) do -- for table
              print(obj[i])          
                -- obj[] nee to have i, not a which it's only dummy used differently
            end
        end
    }
}
```
## This is using keypress enginehandler in `player script` to show player coordinates as ingame messages
```lua
local ui = require('openmw.ui')
local self = require('openmw.self') 
    -- self references the, source or object, which the script is attached to

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.symbol == 'v' then
                local coor = { self.position.x , self.position.y, self.position.z }
                    -- coor needs to be inside here as it's initialised to zero outside of function at load
                for i, a in pairs(coor) do
                  ui.showMessage(tostring(coor[i]))  
                end
            end
        end
    }
}
```
## you can also do this (messaging player current cell) with same requires:
```lua
if key.symbol == 'v' then
    curcell = self.object.cell 
    ui.showMessage(tostring(curcell)) 
        -- or just self.object.cell directly   
end
```
## or with `local types = require('openmw.types')` player's runspeed
```lua
if key.symbol == 'v' then
    local myspeed = types.Actor.runSpeed(self) -- the self's runspeed
    ui.showMessage(tostring(myspeed)) 
end

-- or currently used equipments
if key.symbol == 'v' then
        local inv = types.Actor.equipment(self) -- the self's list of equipment
        for i, a in pairs(inv) do      -- this is needed for table
            print(tostring(inv[i])) 
                -- prints to console instead as there's limited number shown with message
        end
end
```
## onconsume

```lua
local types = require('openmw.types')
local core = require('openmw.core')
local self = require('openmw.self')

local function onConsume(item)  
  if self.cell.name == "Balmora, Nalcarya of White Haven: Fine Alchemist" then
    if item.recordId == "p_levitation_s" then
      types.Actor.spells(self):add("chameleon_60")
    end
  end
end

return { 
      engineHandlers = { onConsume = onConsume }
      } 
```
