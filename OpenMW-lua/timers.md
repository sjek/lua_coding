### savable

```lua
local name_callback = async:registerTimerCallback('arbitary name',
  function(data)  -- data. is used for referencing callback data
      
      core.sendGlobalEvent("event1", { eventdata1 = data.data1 } )
         -- global event in this case to make 1 seconds delay inside time.runRepeatedly
end)

         local function name_function()
            async:newSimulationTimer( 1 , name_callback , { data1 = self }) 
              -- the timer itself. passing self in this case
              -- name_callback as the referenced name
         end

```

### unsavable

- you can't modify dt on onUpdate but you can use it

```lua
local timer = 0
local function onUpdate(dt)
  timer = timer + dt
  if timer > 5 then
    print(timer)
    timer = 0
  end
end
return { engineHandlers = { onUpdate = onUpdate }}
```

- this secures that effect can't be triggered when? timer is running

```lua
local timecheck --outside function/thetrigger

          if timecheck == nil or timecheck == 0 then
            stoptimer2 = time.runRepeatedly(function()
              timecheck = 1
                -- do stuff
           
              if StuffTriggerOrSomething == false then
                stoptimer2()
                timecheck = 0
              end
              
          end, 1*time.second )
         end
```
