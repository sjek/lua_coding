# this is just gotten from console, currently these are hardcoded

[[_TOC_]]

## armor

```
Lua[Player] view(types.Armor.TYPE)

  Helmet = 0,
  Cuirass = 1,
  Greaves = 4,
  RPauldron = 3,
  LPauldron = 2,
  Boots = 5,
  LGauntlet = 6,
  RGauntlet = 7,
  Shield = 8,
  LBracer = 9,
  RBracer = 10  
}
```

## stance
```
Lua[Player] view(types.Actor.stance(self))

neutral = 0
attack  = 1
magic   = 2
```

## magic schools

```
- Lua[Player] view(core.magic.SCHOOL)

 - Alteration = 0,
 - Conjuration = 1,
 - Destruction = 2,
 - Illusion = 3,
 - Mysticism = 4,
 - Restoration = 5
```

## Range

```
- Lua[Player] view(core.magic.RANGE)

-  Self = 0,
-  Touch = 1,
-  Target = 2
```

## magic effects id

```
- Lua[Player] view(core.magic.effects)

- ESM3_MagicEffectStore{141 effects} {

-  0 = ESM3_MagicEffect[sEffectWaterBreathing],
-  1 = ESM3_MagicEffect[sEffectSwiftSwim],
-  2 = ESM3_MagicEffect[sEffectWaterWalking],
-  3 = ESM3_MagicEffect[sEffectShield],
-  4 = ESM3_MagicEffect[sEffectFireShield],
-  5 = ESM3_MagicEffect[sEffectLightningShield],
-  6 = ESM3_MagicEffect[sEffectFrostShield],
-  7 = ESM3_MagicEffect[sEffectBurden],
-  8 = ESM3_MagicEffect[sEffectFeather],
-  9 = ESM3_MagicEffect[sEffectJump],
-  10 = ESM3_MagicEffect[sEffectLevitate],
-  11 = ESM3_MagicEffect[sEffectSlowFall],
-  12 = ESM3_MagicEffect[sEffectLock],
-  13 = ESM3_MagicEffect[sEffectOpen],
-  14 = ESM3_MagicEffect[sEffectFireDamage],
-  15 = ESM3_MagicEffect[sEffectShockDamage],
-  16 = ESM3_MagicEffect[sEffectFrostDamage],
-  17 = ESM3_MagicEffect[sEffectDrainAttribute],
-  18 = ESM3_MagicEffect[sEffectDrainHealth],
-  19 = ESM3_MagicEffect[sEffectDrainSpellpoints],
-  20 = ESM3_MagicEffect[sEffectDrainFatigue],
-  21 = ESM3_MagicEffect[sEffectDrainSkill],
-  22 = ESM3_MagicEffect[sEffectDamageAttribute],
-  23 = ESM3_MagicEffect[sEffectDamageHealth],
-  24 = ESM3_MagicEffect[sEffectDamageMagicka],
-  25 = ESM3_MagicEffect[sEffectDamageFatigue],
-  26 = ESM3_MagicEffect[sEffectDamageSkill],
-  27 = ESM3_MagicEffect[sEffectPoison],
-  28 = ESM3_MagicEffect[sEffectWeaknesstoFire],
-  29 = ESM3_MagicEffect[sEffectWeaknesstoFrost],
-  30 = ESM3_MagicEffect[sEffectWeaknesstoShock],
-  31 = ESM3_MagicEffect[sEffectWeaknesstoMagicka],
-  32 = ESM3_MagicEffect[sEffectWeaknesstoCommonDisease],
-  33 = ESM3_MagicEffect[sEffectWeaknesstoBlightDisease],
-  34 = ESM3_MagicEffect[sEffectWeaknesstoCorprusDisease],
-  35 = ESM3_MagicEffect[sEffectWeaknesstoPoison],
-  36 = ESM3_MagicEffect[sEffectWeaknesstoNormalWeapons],
-  37 = ESM3_MagicEffect[sEffectDisintegrateWeapon],
-  38 = ESM3_MagicEffect[sEffectDisintegrateArmor],
-  39 = ESM3_MagicEffect[sEffectInvisibility],
-  40 = ESM3_MagicEffect[sEffectChameleon],
-  41 = ESM3_MagicEffect[sEffectLight],
-  42 = ESM3_MagicEffect[sEffectSanctuary],
-  43 = ESM3_MagicEffect[sEffectNightEye],
-  44 = ESM3_MagicEffect[sEffectCharm],
-  45 = ESM3_MagicEffect[sEffectParalyze],
-  46 = ESM3_MagicEffect[sEffectSilence],
-  47 = ESM3_MagicEffect[sEffectBlind],
-  48 = ESM3_MagicEffect[sEffectSound],
-  49 = ESM3_MagicEffect[sEffectCalmHumanoid],
-  50 = ESM3_MagicEffect[sEffectCalmCreature],
-  51 = ESM3_MagicEffect[sEffectFrenzyHumanoid],
-  52 = ESM3_MagicEffect[sEffectFrenzyCreature],
-  53 = ESM3_MagicEffect[sEffectDemoralizeHumanoid],
-  54 = ESM3_MagicEffect[sEffectDemoralizeCreature],
-  55 = ESM3_MagicEffect[sEffectRallyHumanoid],
-  56 = ESM3_MagicEffect[sEffectRallyCreature],
-  57 = ESM3_MagicEffect[sEffectDispel],
-  58 = ESM3_MagicEffect[sEffectSoultrap],
-  59 = ESM3_MagicEffect[sEffectTelekinesis],
-  60 = ESM3_MagicEffect[sEffectMark],
-  61 = ESM3_MagicEffect[sEffectRecall],
-  62 = ESM3_MagicEffect[sEffectDivineIntervention],
-  63 = ESM3_MagicEffect[sEffectAlmsiviIntervention],
-  64 = ESM3_MagicEffect[sEffectDetectAnimal],
-  65 = ESM3_MagicEffect[sEffectDetectEnchantment],
-  66 = ESM3_MagicEffect[sEffectDetectKey],
-  67 = ESM3_MagicEffect[sEffectSpellAbsorption],
-  68 = ESM3_MagicEffect[sEffectReflect],
-  69 = ESM3_MagicEffect[sEffectCureCommonDisease],
-  70 = ESM3_MagicEffect[sEffectCureBlightDisease],
-  71 = ESM3_MagicEffect[sEffectCureCorprusDisease],
-  72 = ESM3_MagicEffect[sEffectCurePoison],
-  73 = ESM3_MagicEffect[sEffectCureParalyzation],
-  74 = ESM3_MagicEffect[sEffectRestoreAttribute],
-  75 = ESM3_MagicEffect[sEffectRestoreHealth],
-  76 = ESM3_MagicEffect[sEffectRestoreSpellPoints],
-  77 = ESM3_MagicEffect[sEffectRestoreFatigue],
-  78 = ESM3_MagicEffect[sEffectRestoreSkill],
-  79 = ESM3_MagicEffect[sEffectFortifyAttribute],
-  80 = ESM3_MagicEffect[sEffectFortifyHealth],
-  81 = ESM3_MagicEffect[sEffectFortifySpellpoints],
-  82 = ESM3_MagicEffect[sEffectFortifyFatigue],
-  83 = ESM3_MagicEffect[sEffectFortifySkill],
-  84 = ESM3_MagicEffect[sEffectFortifyMagickaMultiplier],
-  85 = ESM3_MagicEffect[sEffectAbsorbAttribute],
-  86 = ESM3_MagicEffect[sEffectAbsorbHealth],
-  87 = ESM3_MagicEffect[sEffectAbsorbSpellPoints],
-  88 = ESM3_MagicEffect[sEffectAbsorbFatigue],
-  89 = ESM3_MagicEffect[sEffectAbsorbSkill],
-  90 = ESM3_MagicEffect[sEffectResistFire],
-  91 = ESM3_MagicEffect[sEffectResistFrost],
-  92 = ESM3_MagicEffect[sEffectResistShock],
-  93 = ESM3_MagicEffect[sEffectResistMagicka],
-  94 = ESM3_MagicEffect[sEffectResistCommonDisease],
-  95 = ESM3_MagicEffect[sEffectResistBlightDisease],
-  96 = ESM3_MagicEffect[sEffectResistCorprusDisease],
-  97 = ESM3_MagicEffect[sEffectResistPoison],
-  98 = ESM3_MagicEffect[sEffectResistNormalWeapons],
-  99 = ESM3_MagicEffect[sEffectResistParalysis],
-  100 = ESM3_MagicEffect[sEffectRemoveCurse],
-  101 = ESM3_MagicEffect[sEffectTurnUndead],

-  102 = ESM3_MagicEffect[sEffectSummonScamp],
-  103 = ESM3_MagicEffect[sEffectSummonClannfear],
-  104 = ESM3_MagicEffect[sEffectSummonDaedroth],
-  105 = ESM3_MagicEffect[sEffectSummonDremora],
-  106 = ESM3_MagicEffect[sEffectSummonAncestralGhost],
-  107 = ESM3_MagicEffect[sEffectSummonSkeletalMinion],
-  108 = ESM3_MagicEffect[sEffectSummonLeastBonewalker],
-  109 = ESM3_MagicEffect[sEffectSummonGreaterBonewalker],
-  110 = ESM3_MagicEffect[sEffectSummonBonelord],
-  111 = ESM3_MagicEffect[sEffectSummonWingedTwilight],
-  112 = ESM3_MagicEffect[sEffectSummonHunger],
-  113 = ESM3_MagicEffect[sEffectSummonGoldenSaint],
-  114 = ESM3_MagicEffect[sEffectSummonFlameAtronach],
-  115 = ESM3_MagicEffect[sEffectSummonFrostAtronach],
-  116 = ESM3_MagicEffect[sEffectSummonStormAtronach],

-  117 = ESM3_MagicEffect[sEffectFortifyAttackBonus],
-  118 = ESM3_MagicEffect[sEffectCommandCreatures],
-  119 = ESM3_MagicEffect[sEffectCommandHumanoids],

-  120 = ESM3_MagicEffect[sEffectBoundDagger],
-  121 = ESM3_MagicEffect[sEffectBoundLongsword],
-  122 = ESM3_MagicEffect[sEffectBoundMace],
-  123 = ESM3_MagicEffect[sEffectBoundBattleAxe],
-  124 = ESM3_MagicEffect[sEffectBoundSpear],
-  125 = ESM3_MagicEffect[sEffectBoundLongbow],
-  126 = ESM3_MagicEffect[sEffectExtraSpell],
-  127 = ESM3_MagicEffect[sEffectBoundCuirass],
-  128 = ESM3_MagicEffect[sEffectBoundHelm],
-  129 = ESM3_MagicEffect[sEffectBoundBoots],
-  130 = ESM3_MagicEffect[sEffectBoundShield],
-  131 = ESM3_MagicEffect[sEffectBoundGloves],

-  132 = ESM3_MagicEffect[sEffectCorpus],
-  133 = ESM3_MagicEffect[sEffectVampirism],
-  134 = ESM3_MagicEffect[sEffectSummonCenturionSphere],
-  135 = ESM3_MagicEffect[sEffectSunDamage],
-  136 = ESM3_MagicEffect[sEffectStuntedMagicka],
-  137 = ESM3_MagicEffect[sEffectSummonFabricant],
-  138 = ESM3_MagicEffect[sEffectSummonCreature01],
-  139 = ESM3_MagicEffect[sEffectSummonCreature02],
-  140 = ESM3_MagicEffect[sEffectSummonCreature03]
```
