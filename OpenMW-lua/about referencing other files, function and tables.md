## this file is about how to use from other files
1.  tables
2.  and functions 

### functions

- the file declaring the function
```lua
--local self = require('openmw.self')
--local types = require('openmw.types') -- this are for example
 
 local functions = {}       -- the table of functions, lua needs this
 
 functions.speed = function(name) -- !!!!!! this one needs to be structured this way around !!!!!!
                                  -- determines the function with speed keyword inside table
    print("ok")
    --print(types.Actor.getCurrentSpeed(self)) -- for doing stuff
 end
 
 return functions
```
- and in the file using the function

```lua
local functs = require("runspeed.playerspeed") 
            -- this gives the lua file from vfs, folders separated by dots

local function onUpdate()
  functs.speed()    -- uses the function speed from the file
end

return {
  engineHandlers = { onUpdate = onUpdate 
    --function()     -- these can be also declared here
    --functs.speed() -- not needing local function onUpdate end above
    --end
    }
}
```

### tables

- this is simply returning table in a lua file
- basically:
```lua
return {
    [value1] = something,
    [value2] = something2,
    [value3] = something3
}
```
etc.
- and then referencing in file as
```lua
//TODO
```

