## the events in openmw-lua works/starts by simply defining 

- where to send, defined in sender script and then it can send data with it to receiving script(s) / objects
- **you can also do multiple event sents in one file**

[[_TOC_]]

### Defining local event works in the sender like:

`target:sendEvent( "eventname" , { data = value(s), data1 = value(s) } ) `

- **" : "** means self and is preferencing target. this reads as self value in tooltips
- **target** needs to be a GameObject
- **eventname** is the name of the event
- **data** is table or value which can then be used in receiving actor. these **are optional** / not mandatory though.


#### GLOBAL events are the same but don't need target, just the core, ie.

`local core = require('openmw.core')`

`core.sendGlobalEvent( "eventname", { data = value(s) } ) `

- the fields are same as on local event
- notice there is now **" . "** after core


### on sending events to local script (attached somewhere): 

- for this the recipient must have script attached via openmwscripts, 
- directly or via **global** addscript function. using CUSTOM keyword in omwscripts file

- this can be handy for example if you want 
    1. give the script to individual npc, Creature, item, etc.
    2. for npc's for example, script can be attached to specifig race

#### In the receiver then the event is registered trought: 

```lua
return {
    eventHandlers = { eventname = functionname }
}
```

- after this the the data values defined with the event are accessed with:

```lua 
local function functionname(somename)
        print(somename.data)
        print(somename.data1)
end
```
- descriptions: 
- **somename.** can be used to access the event data values using the name given in the event.
- **functionname** is part of the bridge between these two scripts and doesn't do anything else. 

## Example 1

- there's the official doc example
- https://openmw.readthedocs.io/en/latest/reference/lua-scripting/overview.html#event-system 

- And this example here showing multiple events in one script for different keys

- PLAYER script: sending events to nearby actors

```lua
            if key.symbol == 'v' then
              local var1 = self.object
              local acts = nearby.actors
              for b, c in pairs(acts) do
                acts[b]:sendEvent( "eventname1", { data1 = acts[b], data2 = var1 } )
              end
            end
            if key.symbol == 'b' then
              local acts = nearby.actors
              for b, c in pairs(acts) do
                acts[b]:sendEvent( "eventname2", { data3 = acts[b] } )
              end
            end 
```
etc. 

- and in local

```lua
local function testinglua(somename1)
        -- do stuff
end

local function spells(somename2)
        -- do stuff
end

return {
    eventHandlers = { eventname1 = testinglua, eventname2 = spells }
}
```
etc.

## Example 2

- this case in GLOBAL script. 
- note that you can attach script to player in openmwscripts file. 
- not needing the cell search. checks can be done also there 
- TODO: better example

```lua
          local eventdatafromthisscript = "apple pie" --for example
          local cells = world.cells
          for i, _ in pairs(cells) do
            local players = cells[i]:getAll(types.Player)
              for a, _ in pairs(players) do
                players[1]:sendEvent( "eventname" , { eventdataonplayer = eventdatafromthisscript, eventdataonplayer2 = eventdatafromthisscript2 } ) --etc.
              end
          end
```

- and in local player

```lua
local function somerandomname(usedvar)
    ui.showMessage(usedvar.eventdataonplayer)
    --or also soemthing with usedvar.eventdataonplayer2
    -- etc.
end

return { eventHandlers = { eventname = somerandomname } }
```