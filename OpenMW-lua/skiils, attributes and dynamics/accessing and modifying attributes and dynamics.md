### The dynamic (health, magicka, fatigue) and attributes works differently on modifier. 

> dynamic

- **modified value is the new maxinum** but updates to it only when needed
- When doing `types.Actor.stats.dynamic.health(self).modifier = -100` in this case.
- The health has it's modified value separate from current so the value for combat doesn't change before it's below the current maxinum
 
> attributes

- the **change is immediate and can be restored with restore spells / imperial altar**

----------------------------------------------------------------------------

### this script uses timer to diminish speed on intervals

- Other attributes can be used by just writing their name to place of speed in types... 

- This script starts to lessen the player speed (player script) on every 5 seconds since b is pressed.
- note that there's no mechanic preventing extra timers in this script so it's **cumulative**

```lua
local ui = require('openmw.ui')
local self = require('openmw.self')
local types = require('openmw.types')
local time = require('openmw_aux.time')

return {
    engineHandlers = {
        onKeyPress = function(key)
            if key.symbol == 'b' then
                 local test = types.Actor.stats.attributes.speed(self) 
                        -- this fetches the attribute 
                        -- which can be modified with .modifier = 
                 local stopFn = time.runRepeatedly(
                      function() 
                        -- do stuff
                        ui.showMessage(tostring(test))
                        test.modifier = -10
                      end,
                      5 * time.second) -- every 5 seconds
            end
        end
    }
}
```
