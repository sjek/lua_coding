### this will simply play sound when near book(s)

BOOK script
- if wanted for other object types, global script with ``objectactive(object)`` can be used
- or more keywords to omwscripts file
- this checks per player so possibly multiplayer safe

```lua
local types = require('openmw.types')
local core = require('openmw.core')
local self = require('openmw.self')
local nearby = require('openmw.nearby')
local time = require('openmw_aux.time')

-- journal MS_FargothRing 10
-- is when you have talked to fargoth

local function onActive()
   if self.recordId == string.lower("bk_BriefHistoryEmpire4") then -- the object to be near to

    local lp = nearby.players
      for i, _ in pairs(lp) do -- for every player
        if types.Player.quests(lp[i])["MS_FargothRing"].stage ~= 10 then -- the quest stage for which not to play
          timer = time.runRepeatedly(function()
            if ( lp[i].position - self.position ):length() < 300 then  
                    -- at sound hearing distance, is this necessary ?
          
                 core.sound.playSoundFile3d("Sound\\Fx\\icecracking.wav",self, { -- soundfile to play

                volume = ( 300 / ( lp[i].position - self.position ):length() ) })         
                                  -- fades to distance of 300
              print("sound")
           end
           
          if types.Player.quests(lp[i])["MS_FargothRing"].stage == 10 then
            timer()         -- cuts the sound on quest stage, inside timer
            print("stop by quest")
          end
           
        end, 10*time.second ) -- repeat every ten seconds        
       end
    end
  end
end

local function onInactive()
  if timer then
  timer()   -- stop the tiemr when cell is left
  print("stop")
  end
end

return { engineHandlers = { onActive = onActive, onInactive = onInactive } }
```