local core = require('openmw.core')
local self = require('openmw.self')


local questid = { MS_FargothRing = 10 }
local function onQuestUpdate( questid )
               core.sendGlobalEvent( "eventname", { playerself=self } )
end

return { engineHandlers = { onQuestUpdate = onQuestUpdate } } 
