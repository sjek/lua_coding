### these all are not particularly optimized and have been done in some months so needs alterations here and there, like the event names.

#### WIP: list of made things

- for now i just sum up some what have been done. 

##### animations and vfx

-   placing vfx

    1. to nearby targets
    2. to hands while spell is chosen

-   a slow mo 

##### inventories

-   this contains stuff with both containers and actors. 
    1.  get / setequipments 
    2.  instant transmission of item to containers upon activation or not so instant
    3.  copying player equipment to actors
    4.  triggering by having something in inventory


##### magic

-   here's simply magic related functions and advice how to deal with them

    1.  giving spells to actors
        1. all spells
        2. specifig spells
        3. making all attacks hit with fortifyattack
        4. chameleon while sneaking

    2.  print stuff
        1. printing the nearby actors spells to console

    3.  using the being under spell as a trigger
        1. placing bubbles under night eye upon containers

##### npc behaviour

-   how to us AI package, clearance of self.controls and such

##### quest and stuff

-   nothing yet, simply how to use onQuestUpdate enginehandler

##### raycast

-   barebones examples of usage

-   way to do castRay, castRenderingRay and asyncCastRenderingRay with different effects
    1.  teleporting player towards hitpos 
    2.  placing atronach at hitpos
    3.  commanding npc's with swinging weapon and using target info
    4.  listing different uses like stealing spells

##### skills, attributes and dynamics

-   just modifying speed atm

##### world

-   this is a bit larger one
    1.  record creation
    2.  keys detection
    3.  accessing cell content
    4.  making a grid of objects
    5.  using onActive and onInactive, to place and remove stuff
    6.  removing stuff
    7.  object replacement
    8.  and scaling stuff
